0.1.6

* nbfront does not exit quietly if an unexpected exception is thrown.

0.1.5

* Print related LR items in conflict reports.
* Fixed problem running on Windows.
* Set default parser name based on file name.

0.1.4

* Improved error reporting.
* Improved Beaver compatibility.
* Implemented Beaver's explicit rule precedence system.

0.1.3

  * Improved automatic component names to avoid name collisions for tokens.

0.1.2

  * Fixed error causing unused terminals to not be generated in Beaver parser.

0.1.1

  * Fixed problem where the REDUCE action in a SHIFT/REDUCE conflict was
    selected.  The SHIFT action should now be the default resolution.
