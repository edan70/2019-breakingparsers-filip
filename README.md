# NeoTestGenerator

This is a test-generator tool built on top of the [NeoBeaver][1] parser generator.
It is used to easily generate a test suite for a parser based upon its grammar.

Test generation algorithms implemented:

* Purdom's algorithm for rule coverage.
* Raselimo et al. word mutation algorithm.
* Raselimo et al. rule mutation algorithm.

## How to run it

Use the following commands to generate a test suite:

    gradle jar
    java -jar neobeaver.jar --generatetests <grammar>

Where <grammar> is the Beaver grammar file. The test suite will be outputed into a 
directory with the named "Generated tests for <grammar>". If such directory 
already exists it will be deleted, so be careful. If the input file is named 
"parser.beaver", then the tests will be pretty printed to SimpliC.

## Examples

In the folder `neotestgenerator examples/` there are examples. Run the .jar file on
these .beaver files to see the result.

## How to test it

Use the following command to test the tool:

    gradle test

## Credits

This tool is built on top of the [NeoBeaver][1] parser generator which is licensed
under the Modified BSD License.

## License

Copyright (c) 2019, Filip Johansson

This project is provided under the license BSD 2-clause.
See the LICENSE file, in the same directory as this README file.


[1]: http://extendj.org/neobeaver.html
