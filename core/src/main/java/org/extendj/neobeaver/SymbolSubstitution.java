/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import org.extendj.neobeaver.ast.ASTNode;
import org.extendj.neobeaver.ast.GComponent;
import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.Opt;
import org.extendj.neobeaver.RuleMutation;

public class SymbolSubstitution extends RuleMutation {

	private GRule rule;
	private int index;
	private String sym;

	public SymbolSubstitution(GRule rule, int index, String sym) {
		this.index = index;
		this.rule = rule;
		this.sym = sym;
	}

	@Override
	public GGrammar mutate(GGrammar grammar) {
		GGrammar mutatedGrammar = grammar.treeCopy();
		GComponent insertedGComp = new GComponent(sym, new Opt(), "");

		for (GRule rule : mutatedGrammar.rules()) {
			if (this.isTheSameRule(this.rule, rule)) {
				this.mutatedRule = rule;

				rule.getGComponents().removeChild(index);
				rule.getGComponents().insertChild(insertedGComp, index);
				return mutatedGrammar;
			}
		}

		throw new RuntimeException("Rule not found");
	}

	@Override 
	public String comment() {
		String comm = " // This is a symbol subsitution of rule: ";
		comm += rule.toString();
		comm += ", with symbol: " + sym;
		comm += ", at index: " + index;
		comm += "\n";

		return comm;
	}

}
