/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import org.extendj.neobeaver.ast.ASTNode;
import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GComponent;

import org.extendj.neobeaver.WordMutation;

public class GenWordMutations {

	public static List<WordMutation> generateWordMutations(GGrammar grammar,
			List<String> tokens) {

		List<WordMutation> mutations = new LinkedList<WordMutation>();
		int length = tokens.size();

		for (int i = 0; i < length; i++) {
			// token deletion
			if (i < length - 2) {
				if (grammar.isPP(tokens.get(i), tokens.get(i+2))) {
					mutations.add(new TokenDeletion(i+1));
				}
			}

			// token insertion
			for (String term : grammar.terminals()) {
				if (grammar.isPP(tokens.get(i), term) 
						|| i < length - 1 
						&& grammar.isPP(term, tokens.get(i+1))) {
					mutations.add(new TokenInsertion(i+1, term));
				}
			}

			// token substitution
			if (i < length - 1) {
				for (String term : grammar.terminals()) {
					if (grammar.isPP(tokens.get(i), term) 
							|| i < length - 2 
							&& grammar.isPP(term, tokens.get(i+2))) {
						mutations.add(new TokenSubstitution(i+1, term));
					}
				}
			}

			// token transposition
			if (i < length - 2) {
				if (grammar.isPP(tokens.get(i), tokens.get(i+2)) 
						|| grammar.isPP(tokens.get(i+2), tokens.get(i+1))
						|| i < length - 3
						&& grammar.isPP(tokens.get(i+1), tokens.get(i+3))) {
					mutations.add(new TokenTransposition(i+1, i+2));
				}
			}
		}

		return mutations;
	}

}
