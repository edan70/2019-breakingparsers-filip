package org.extendj.neobeaver;

/**
 * Tracks if any errors happen within a scope.
 */
public class ProblemScope implements ProblemHandler {
  private ProblemHandler parent;
  private boolean errored = false;

  public ProblemScope(ProblemHandler parent) {
    this.parent = parent;
  }

  @Override public void error(String message) {
    parent.error(message);
    errored = true;
  }

  @Override
  public void errorf(String fmt, Object... args) {
    parent.errorf(fmt, args);
    errored = true;
  }

  @Override public void warn(String message) {
    parent.warn(message);
  }

  @Override
  public void warnf(String fmt, Object... args) {
    parent.warnf(fmt, args);
  }

  @Override
  public boolean errored() {
    return errored;
  }
}
