package org.extendj.neobeaver;

public interface TraceEvent extends AutoCloseable {
  @Override void close();
}
