/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExtensionBuilderLr1 {

  private final Map<Symbol, Collection<Rule>> byLhs;
  private final BitSet nullable;
  private final Map<Symbol, Set<Symbol>> first;
  private final Map<Symbol, Set<Symbol>> follow;
  private final Map<Tuple<Symbol,Symbol>, Set<Item>> cache = new HashMap<>();

  public ExtensionBuilderLr1(Map<Symbol, Collection<Rule>> byLhs, BitSet nullable,
      Map<Symbol, Set<Symbol>> first, Map<Symbol, Set<Symbol>> follow) {
    this.byLhs = byLhs;
    this.nullable = nullable;
    this.first = first;
    this.follow = follow;
  }

  public Collection<? extends Item> extension(Symbol sym, Symbol lookahead) {
    Tuple<Symbol, Symbol> key = new Tuple<>(sym, lookahead);
    if (cache.containsKey(key)) {
      // NB: This is circular.
      return cache.get(key);
    }
    cache.put(key, new HashSet<Item>());
    while (true) {
      Set<Item> nextExtension = new HashSet<>();
      for (Rule rule : byLhs.get(sym)) {
        ItemLr1 item = new ItemLr1(rule, 0, lookahead);
        nextExtension.add(item);
        if (!rule.rhs.isEmpty()) {
          Symbol x = rule.rhs.get(0);
          if (!x.isTerminal()) {
            for (Symbol y : itemFollows(item)) {
              nextExtension.addAll(extension(x, y));
            }
          }
        }
      }

      if (!cache.get(key).addAll(nextExtension)) {
        break;
      }
    }
    return cache.get(key);
  }

  private List<Symbol> itemFollows(ItemLr1 item) {
    List<Symbol> set = new ArrayList<>();
    for (int i = item.dot + 1; i < item.rule.rhs.size(); ++i) {
      Symbol x = item.rule.rhs.get(i);
      set.addAll(first.get(x));
      if (x.isTerminal() || !nullable.get(x.id())) {
        return set;
      }
    }
    set.add(item.follow);
    return set;
  }
}
