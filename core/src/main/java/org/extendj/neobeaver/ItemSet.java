/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ItemSet {
  private final int id;
  public final Set<Item> items;
  public final Set<Item> extension;
  public final ItemSet core;

  public ItemSet(int id, Set<Item> items) {
    this.id = id;
    this.items = items;
    this.extension = Collections.emptySet();
    core = this;
  }

  public ItemSet(int id, ItemSet core, Set<Item> extension) {
    this.id = id;
    this.items = core.items;
    this.extension = extension;
    this.core = core;
  }

  public int id() {
    return id;
  }

  @Override public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append(String.format("item set S%d%n", id));
    buf.append(setToString("  ", items));
    buf.append(setToString("+ ", extension));
    return buf.toString();
  }

  private String setToString(String prefix, Set<Item> set) {
    Map<Item, Collection<Symbol>> followSets = new HashMap<>();
    for (Item item : set) {
      Item base = item.baseItem();
      Collection<Symbol> syms = followSets.get(base);
      if (syms == null) {
        syms = new HashSet<>();
        followSets.put(base, syms);
      }
      syms.addAll(item.followSyms());
    }
    List<String> items = new ArrayList<>();
    for (Map.Entry<Item, Collection<Symbol>> entry : followSets.entrySet()) {
      String follows = "";
      Item item = entry.getKey();
      Collection<Symbol> followSyms = entry.getValue();
      if (!followSyms.isEmpty()) {
        StringBuilder buf = new StringBuilder();
        for (Symbol follow : followSyms) {
          if (buf.length() > 0) {
            buf.append(", ");
          }
          buf.append(Util.escape(follow.toString()));
        }
        follows = String.format(" [%s]", buf.toString());
      }
      items.add(String.format("%s%s%s%n", prefix, Util.escape(item.toString()), follows));
    }
    Collections.sort(items);
    StringBuilder buf = new StringBuilder();
    for (String item : items) {
      buf.append(item);
    }
    return buf.toString();
  }

  public Collection<Tuple<Symbol, ItemSet>> followCores() {
    Collection<Tuple<Symbol, ItemSet>> result = new ArrayList<>();
    List<Item> advanced = new ArrayList<>();
    for (Item item : items) {
      if (item.canAdvance()) {
        advanced.add(item.advance());
      }
    }
    for (Item item : extension) {
      if (item.canAdvance()) {
        advanced.add(item.advance());
      }
    }
    Map<Symbol, Set<Item>> map = new HashMap<>();
    for (Item item : advanced) {
      Symbol sym = item.rule.rhs.get(item.dot - 1);
      Set<Item> set = map.get(sym);
      if (set == null) {
        set = new HashSet<>();
        map.put(sym, set);
      }
      set.add(item);
    }
    for (Map.Entry<Symbol, Set<Item>> entry : map.entrySet()) {
      result.add(new Tuple<>(entry.getKey(), new ItemSet(-1, entry.getValue())));
    }
    return result;
  }

  @Override public int hashCode() {
    int hash = 0;
    for (Item item : items) {
      hash ^= item.hashCode();
    }
    return hash;
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof ItemSet) {
      ItemSet other = (ItemSet) obj;
      return other.items.containsAll(items) && items.containsAll(other.items);
    }
    return false;
  }

  public ItemSet core() {
    return core;
  }

  public ItemSet merge(Set<Item> other, Set<Item> extension, Grammar grammar) {
    Set<Item> newExtension = new HashSet<>(this.extension);
    newExtension.addAll(extension);
    Set<Item> coreItems = new HashSet<>(items);
    coreItems.addAll(other);
    ItemSet newCore = new ItemSet(id, coreItems);
    return new ItemSet(id, newCore, newExtension);
  }

  public void printGraphNode() {
    System.out.format("  S%d[label=\"\\N\\n", id());
    printDotSet("  ", items);
    printDotSet("+ ", extension);
    System.out.println("\"];");
  }

  private void printDotSet(String prefix, Set<Item> set) {
    Map<Item, Collection<Symbol>> followSets = new HashMap<>();
    for (Item item : set) {
      Item base = item.baseItem();
      Collection<Symbol> syms = followSets.get(base);
      if (syms == null) {
        syms = new HashSet<>();
        followSets.put(base, syms);
      }
      syms.addAll(item.followSyms());
    }
    List<String> items = new ArrayList<>();
    for (Map.Entry<Item, Collection<Symbol>> entry : followSets.entrySet()) {
      String follows = "";
      Item item = entry.getKey();
      Collection<Symbol> followSyms = entry.getValue();
      if (!followSyms.isEmpty()) {
        StringBuilder buf = new StringBuilder();
        for (Symbol follow : followSyms) {
          if (buf.length() > 0) {
            buf.append(", ");
          }
          buf.append(Util.escape(follow.toString()));
        }
        follows = String.format(" [%s]", buf.toString());
      }
      items.add(String.format("%s%s%s\\l", prefix, Util.escape(item.toString()), follows));
    }
    Collections.sort(items);
    for (String item : items) {
      System.out.print(item);
    }
  }

  public ItemSet baseCore() {
    Set<Item> baseItems = new HashSet<>();
    for (Item item : items) {
      baseItems.add(item.baseItem());
    }
    return new ItemSet(-1, baseItems);
  }

  public Collection<Item> allItems() {
    Collection<Item> all = new ArrayList<>();
    all.addAll(items);
    all.addAll(extension);
    return all;
  }

  public Collection<Item> relatedItems(Symbol sym) {
    Collection<Item> result = new ArrayList<>();
    List<Item> advanced = new ArrayList<>();
    for (Item item : items) {
      if (item.related(sym)) {
        result.add(item);
      }
    }
    for (Item item : extension) {
      if (item.related(sym)) {
        result.add(item);
      }
    }
    return result;
  }
}
