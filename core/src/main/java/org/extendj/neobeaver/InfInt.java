/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

public class InfInt {

	private boolean isInf;
	private int value;

	public InfInt(int val) {
		value = val;
		isInf = false;
	}

	public InfInt(boolean isInf) {
		value = 0;
		this.isInf = isInf;
	}

	public void setPosInf(boolean isInf) {
		this.isInf = isInf;
	}

	public void add(InfInt num) {
		value += num.value;
		isInf = isInf || num.isInf;
	}

	public void sub(InfInt num) {
		value -= num.value;
		isInf = isInf || num.isInf; 
	}

	public boolean isInf() {
		return isInf;
	}

	public boolean lessThan(InfInt num) {
		return (value < num.value && !(isInf || num.isInf))
				|| (!isInf && num.isInf);
	}

	public InfInt copy() {
		InfInt tmp = new InfInt(0);
		tmp.isInf = isInf;
		tmp.value = value;

		return tmp;
	}

	public int getValue() {
		return value;
	}

}
