/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

public class SimpliCPrettyPrinting {
	private static String indent = "";
	private static boolean indentNext = false;
	private static boolean spaceNext = false;
	private static boolean prevID = false;
	private static boolean prevNUMERAL = false;

	public static String prettyPrint(String token) {
		if (indentNext) {
			indentNext = false;
			String lookup = lookup(token);
			return indent + lookup;
		} else if (spaceNext) {
			spaceNext = false;
			return " " + lookup(token);
		} else if (prevID) {
			prevID = false;
			String potSpace = lookupPrevID(token);
			return potSpace + lookup(token);
		} else if (prevNUMERAL) {
			prevNUMERAL = false;
			String potSpace = "";
			if (token.equals("NUMERAL")) {
				potSpace = " ";
			}
			return potSpace + lookup(token);
		}

		return lookup(token);

	}

	public static void resetIndent() {
		indent = "";
		indentNext = false;

		spaceNext = false;
		prevID = false;
	}

	private static String lookup(String token) {

		switch (token) {
			case "INT":
				spaceNext = true;
				return "int";
			case "IF":
				spaceNext = true;
				return "if";
			case "WHILE":
				spaceNext = true;
				return "while";
			case "ELSE":
				spaceNext = true;
				return "else";
			case "LEFTPAR":
				return "(";
			case "RIGHTPAR":
				return ")";
			case "LEFTBRACE":
				indent += "\t";
				indentNext = true;
				return " {\n";
			case "RIGHTBRACE":
				if (indent.length() > 0) {
					indent = indent.substring(0, indent.length() - 1);
				}
				indentNext = true;
				return "}\n";
			case "ASSIGN":
				spaceNext = true;
				return " =";
			case "SEMICOL":
				indentNext = true;
				return ";\n";
			case "COMMA":
				spaceNext = true;
				return ",";
			case "ADD":
				spaceNext = true;
				return " +";
			case "SUB":
				spaceNext = true;
				return " -";
			case "MUL":
				spaceNext = true;
				return " *";
			case "DIV":
				spaceNext = true;
				return " /";
			case "REST":
				spaceNext = true;
				return " %";
			case "EQUAL":
				spaceNext = true;
				return " ==";
			case "NOTEQUAL":
				spaceNext = true;
				return " !=";
			case "LESSEQUAL":
				spaceNext = true;
				return " <=";
			case "LESS":
				spaceNext = true;
				return " <";
			case "GREATEREQUAL":
				spaceNext = true;
				return " >=";
			case "GREATER":
				spaceNext = true;
				return " >";
			case "RETURN":
				spaceNext = true;
				return "return";
			case "ID":
				prevID = true;
				return "a";
			case "NUMERAL":
				prevNUMERAL = true;
				return "1";
			default:
				throw new RuntimeException("No such token: <" + token + ">");
		}

	}

	private static String lookupPrevID(String token) {

		switch (token) {
			case "INT":
				return " ";
			case "IF":
				return " ";
			case "WHILE":
				return " ";
			case "ELSE":
				return " ";
			case "RETURN":
				return " ";
			case "ID":
				return " ";
			case "NUMERAL":
				return " ";
			default:
				return "";
		}

	}

}
