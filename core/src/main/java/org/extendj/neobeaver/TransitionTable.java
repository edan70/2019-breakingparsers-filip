/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.extendj.neobeaver.Grammar.VERBOSE;

public class TransitionTable {
  public final ItemSet goal;
  public final Map<ItemSet, Map<Symbol, ItemSet>> map;
  public final Map<ItemSet, Map<Symbol, Action>> actions;
  private final Map<ItemSet, Map<Symbol, List<Action>>> actionMap;
  public final List<Conflict> conflicts;

  public TransitionTable(
      ItemSet goal, Map<ItemSet, Map<Symbol, ItemSet>> map,
      Map<ItemSet, Map<Symbol, Action>> actions,
      Map<ItemSet, Map<Symbol, List<Action>>> actionMap, List<Conflict> conflicts) {
    this.goal = goal;
    this.map = map;
    this.actions = actions;
    this.actionMap = actionMap;
    this.conflicts = conflicts;
  }

  public static TransitionTable build(Grammar grammar,
      ProblemHandler problems,
      Set<Symbol> syms, List<ItemSet> itemSets,
      ItemSet goal, Map<ItemSet, ItemSet> coreMap,
      List<Tuple3<ItemSet, ItemSet, Symbol>> transitions,
      List<Tuple3<ItemSet, Symbol, Action>> extraActions) {
    Map<ItemSet, Map<Symbol, ItemSet>> map = new HashMap<>();
    Map<ItemSet, Map<Symbol, List<Action>>> actions = new HashMap<>();
    // Initialize action map.
    for (ItemSet set : itemSets) {
      map.put(set, new HashMap<Symbol, ItemSet>());
      actions.put(set, new HashMap<Symbol, List<Action>>());
    }
    for (Tuple3<ItemSet, ItemSet, Symbol> transition : transitions) {
      ItemSet source = coreMap.get(transition.first);
      ItemSet dest = coreMap.get(transition.second);
      Symbol sym = transition.third;
      map.get(source).put(sym, dest);
    }
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Symbol sym : syms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }
    for (ItemSet set : itemSets) {
      for (Item item : set.allItems()) {
        if (item.dot >= item.rule.rhs.size()) {
          Collection<Tuple3<ItemSet, Symbol, Action>> reduceActs = item.reduceActions(grammar, set);
          for (Tuple3<ItemSet, Symbol, Action> action : reduceActs) {
            addAction(actions, action.first, action.second, action.third);
          }
        }
      }
      Map<Symbol, ItemSet> setTransitions = map.get(set);
      for (Symbol sym : terminals) {
        if (setTransitions.containsKey(sym)) {
          addAction(actions, set, sym, new Shift(sym, setTransitions.get(sym)));
        }
      }
    }
    for (Tuple3<ItemSet, Symbol, Action> action : extraActions) {
      addAction(actions, action.first, action.second, action.third);
    }
    List<Conflict> conflicts = new LinkedList<>();
    Map<ItemSet, Map<Symbol, Action>> actionMap = buildActionMap(grammar, problems, actions, conflicts);
    return new TransitionTable(goal, map, actionMap, actions, conflicts);
  }

  private static Map<ItemSet, Map<Symbol, Action>> buildActionMap(
      Grammar grammar,
      ProblemHandler problems,
      Map<ItemSet, Map<Symbol, List<Action>>> actionMap, List<Conflict> conflicts) {
    Map<ItemSet, Map<Symbol, Action>> actions = new HashMap<>();
    for (Map.Entry<ItemSet, Map<Symbol, List<Action>>> entry : actionMap.entrySet()) {
      ItemSet itemSet = entry.getKey();
      Map<Symbol, List<Action>> inMap = entry.getValue();
      Map<Symbol, Action> outMap = new HashMap<>();
      actions.put(itemSet, outMap);
      for (Map.Entry<Symbol, List<Action>> actionEntry : inMap.entrySet()) {
        Symbol sym = actionEntry.getKey();
        List<Action> setActions = actionEntry.getValue();
        if (setActions.size() > 1) {
          boolean change = true;
          while (change) {
            change = false;
            Action action = setActions.get(0);
            for (int i = 1; i < setActions.size(); ++i) {
              Action otherAction = setActions.get(i);
              Resolution selected = resolve(problems, itemSet, action, otherAction, grammar, sym);
              if (selected == Resolution.FIRST) {
                setActions.remove(i);
                change = true;
                break;
              } else if (selected == Resolution.SECOND) {
                setActions.remove(0);
                change = true;
                break;
              }
            }
          }
          Action action = setActions.get(0);
          for (int i = 1; i < setActions.size(); ++i) {
            Action otherAction = setActions.get(i);
            conflicts.add(new Conflict(itemSet, sym, action, otherAction));
          }
        }
        outMap.put(actionEntry.getKey(), setActions.get(0));
      }
    }
    return actions;
  }

  private static void addAction(Map<ItemSet, Map<Symbol, List<Action>>> actionMap,
      ItemSet set, Symbol sym, Action action) {
    Map<Symbol, List<Action>> setActions = actionMap.get(set);
    List<Action> actionSet = setActions.get(sym);
    if (actionSet == null) {
      actionSet = new LinkedList<>();
      setActions.put(sym, actionSet);
    }
    actionSet.add(action);
  }

  public void checkProblems(Grammar grammar, ProblemHandler problems, Set<Symbol> unused, boolean unreachableError) {
    Set<Rule> reduceRules = new HashSet<>();
    for (Map.Entry<ItemSet, Map<Symbol, List<Action>>> entry : actionMap.entrySet()) {
      for (List<Action> actionList : entry.getValue().values()) {
        for (Action action : actionList) {
          if (action.kind() == ActionKind.REDUCE) {
            reduceRules.add(((Reduce) action).rule);
          }
        }
      }
    }
    for (Rule rule : grammar.rules) {
      if (rule.lhs != Symbol.GOAL && !unused.contains(rule.lhs) && !reduceRules.contains(rule)) {
        String message = "rule can not be reduced: " + rule;
        if (unreachableError) {
          problems.error(message);
        } else {
          problems.warn(message);
        }
      }
    }
  }

  enum Resolution {
    FIRST, SECOND, NONE
  }

  enum SelectedAction {
    SHIFT, REDUCE, NONE
  }

  /** Choose between conflicting actions using precedence. */
  public static Resolution resolve(ProblemHandler problems, ItemSet set, Action firstAction, Action secondAction,
      Grammar grammar, Symbol sym) {
    String first = grammar.precedenceTerminal(firstAction);
    String second = grammar.precedenceTerminal(secondAction);
    if (grammar.precedence(first) < grammar.precedence(second)) {
      return Resolution.FIRST;
    } else if (grammar.precedence(first) > grammar.precedence(second)) {
      return Resolution.SECOND;
    } else if (grammar.nonassoc(first) || grammar.nonassoc(second)) {
      // Non-associative tokens may not be part in any conflict.
      return Resolution.NONE;
    } else {
      SelectedAction selected;
      if (grammar.left(first) && grammar.left(second)) {
        selected = SelectedAction.REDUCE;
      } else if (grammar.right(first) && grammar.right(second)) {
        selected = SelectedAction.SHIFT;
      } else {
        if (firstAction.kind() == ActionKind.SHIFT && secondAction.kind() == ActionKind.REDUCE
            || firstAction.kind() == ActionKind.REDUCE && secondAction.kind() == ActionKind.SHIFT) {
          // TODO: improve shift/reduce handling (add option to convert warning to error).
          StringBuilder sb = new StringBuilder();
          sb.append(String.format(
              "resolved SHIFT/REDUCE conflict on [%s] by selecting SHIFT:%n" +
              "  %s%n" +
              "  %s", sym, firstAction, secondAction));
          sb.append(String.format("%nContext:"));
          for (Item it : set.relatedItems(sym)) {
            sb.append(String.format("%n  %s", it));
          }
          problems.warn(sb.toString());
          selected = SelectedAction.SHIFT;
        } else {
          selected = SelectedAction.NONE;
        }
      }
      ActionKind kind = firstAction.kind();
      ActionKind otherKind = secondAction.kind();
      if (selected == SelectedAction.SHIFT) {
        if (kind == ActionKind.SHIFT && otherKind == ActionKind.REDUCE) {
          return Resolution.FIRST;
        } else if (kind == ActionKind.REDUCE && otherKind == ActionKind.SHIFT) {
          return Resolution.SECOND;
        }
      }
      if (selected == SelectedAction.REDUCE) {
        if (kind == ActionKind.SHIFT && otherKind == ActionKind.REDUCE) {
          return Resolution.SECOND;
        } else if (kind == ActionKind.REDUCE && otherKind == ActionKind.SHIFT) {
          return Resolution.FIRST;
        }
      }
    }
    return Resolution.NONE;
  }

  public void printTables(Grammar grammar, List<ItemSet> itemSets) {
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Symbol sym : grammar.syms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }

    System.out.println("Transition Table");
    System.out.println("----------------");
    for (Symbol sym : grammar.syms) {
      System.out.print("\t" + sym + ",");
    }
    System.out.println();
    for (ItemSet set : itemSets) {
      Map<Symbol, ItemSet> transitions = map.get(set);
      System.out.print("S" + set.id());
      for (Symbol sym : grammar.syms) {
        if (transitions.containsKey(sym)) {
          System.out.print("\tS" + transitions.get(sym).id() + ",");
        } else {
          System.out.print("\t,");
        }
      }
      System.out.println();
    }
    System.out.println();
    System.out.println("Action Table");
    System.out.println("------------");
    for (Symbol sym : terminals) {
      System.out.print("\t" + sym + ",");
    }
    System.out.println();
    for (ItemSet set : itemSets) {
      System.out.print("S" + set.id());
      Map<Symbol, Action> setActions = actions.get(set);
      for (Symbol sym : terminals) {
        if (setActions.containsKey(sym)) {
          Action action = setActions.get(sym);
          System.out.print("\t" + action.code() + ",");
        } else {
          System.out.print("\t,");
        }
      }
      System.out.println();
    }
    System.out.println();
    System.out.println("Goto Table");
    System.out.println("----------");
    for (Symbol sym : nonterminals) {
      System.out.print("\t" + sym + ",");
    }
    System.out.println();
    for (ItemSet set : itemSets) {
      Map<Symbol, ItemSet> transitions = map.get(set);
      System.out.print("S" + set.id());
      for (Symbol sym : nonterminals) {
        if (transitions.containsKey(sym)) {
          System.out.print("\tS" + transitions.get(sym).id() + ",");
        } else {
          System.out.print("\t,");
        }
      }
      System.out.println();
    }
  }

  public void printConflicts(ProblemHandler problems, Grammar grammar) {
    Map<ItemSet, Collection<Conflict>> map = new HashMap<>();
    for (Conflict conflict : conflicts) {
      if (!map.containsKey(conflict.set)) {
        map.put(conflict.set, new ArrayList<Conflict>());
      }
      map.get(conflict.set).add(conflict);
    }
    StringBuilder message = new StringBuilder();
    if (!map.isEmpty()) {
      message.append("grammar contains conflicts:");
    }
    for (Map.Entry<ItemSet, Collection<Conflict>> entry : map.entrySet()) {
      for (Conflict conflict : entry.getValue()) {
        message.append(String.format("%n  %s", conflict));
      }
      ItemSet set = entry.getKey();
      Conflict first = entry.getValue().iterator().next();
      message.append(String.format("%nContext:"));
      for (Item it : set.relatedItems(first.sym)) {
        message.append(String.format("%n  %s", it));
      }
    }
    if (message.length() > 0) {
      problems.error(message.toString());
    }
    for (Conflict conflict : conflicts) {
      if (grammar.nonassoc(conflict.sym.name())) {
        problems.errorf(
            "Symbol %s is non-associative, but there are conflicts including the symbol.",
            conflict.sym.name());
      }
    }
  }
}
