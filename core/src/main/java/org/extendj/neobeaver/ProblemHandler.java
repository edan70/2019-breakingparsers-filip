package org.extendj.neobeaver;

public interface ProblemHandler {
  void error(String message);

  void errorf(String fmt, Object... args);

  void warn(String message);

  void warnf(String fmt, Object... args);

  /** Return {@code true} if there were any errors reported. */
  boolean errored();
}
