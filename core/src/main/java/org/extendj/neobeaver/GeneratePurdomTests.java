/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.List;
import java.util.LinkedList;

import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GComponent;

public class GeneratePurdomTests {

	private static String currSym;

	private static List<List<String>> output;

	private static Map<String, List<String>> onceStrings;
	private static Map<GRule, List<String>> markStrings;
	private static Map<String, List<String>> onstStrings;

	public static List<List<String>> generatePurdomTests(GGrammar grammar) {
		// shortest derivable length
		Map<String, InfInt> SLEN = new HashMap<String, InfInt>();
		// rule for shortest derivable length  
		Map<String, GRule> SHORT = new HashMap<String, GRule>(); 
		// shortest left length 
		Map<GRule, InfInt> RLEN = new HashMap<GRule, InfInt>();

		shortestTerminalString(grammar, SLEN, SHORT, RLEN);


		// shortest sentence length
		Map<String, InfInt> DLEN = new HashMap<String, InfInt>();
		// intro rule  
		Map<String, GRule> PREV = new HashMap<String, GRule>();

		shortestDerivation(grammar, SLEN, RLEN, DLEN, PREV);


		// used   
		Map<GRule, Boolean> MARK = new HashMap<GRule, Boolean>(); 
		// number on stack
		Map<String, Integer> ONST = new HashMap<String, Integer>(); 
		// next rule
		Map<String, GRule> ONCE = new HashMap<String, GRule>(); 
		// STACK
		Stack<String> STACK = new Stack<String>(); 

		output = new LinkedList<List<String>>();

		onceStrings = new HashMap<String, List<String>>();
		markStrings = new HashMap<GRule, List<String>>();
		onstStrings = new HashMap<String, List<String>>();

		for (String nonTerm : grammar.nonTerminals()) {
			onceStrings.put(nonTerm, new LinkedList<String>());
			onstStrings.put(nonTerm, new LinkedList<String>());
		}
		for (GRule rule : grammar.rules()) {
			markStrings.put(rule, new LinkedList<String>());
		}

		sentenceGeneration(grammar, SHORT, PREV, MARK, ONST, ONCE, STACK);

		// unccomment to print information, can be used for debugging
		// the info will printed last of the (purdom) tests
		//printSLEN(grammar);
		//printSHORT(grammar);
		//printRLEN(grammar);
		//printDLEN(grammar);
		//printPREV(grammar);
		//printONCE(grammar);
		//printMARK(grammar);
		//printONST(grammar);

		return output;
	}

	public static void shortestTerminalString (GGrammar grammar, 
			Map<String, InfInt> SLEN, Map<String, GRule> SHORT, 
			Map<GRule, InfInt> RLEN) {

		// Initialize
		for (String term : grammar.terminals()) {
			SLEN.put(term, new InfInt(1));
		}
		for (String nonTerm : grammar.nonTerminals()) {
			SLEN.put(nonTerm, new InfInt(true));
			SHORT.put(nonTerm, null);
		}
		for (GRule rule : grammar.rules()) {
			RLEN.put(rule, new InfInt(true));
		}

		// Try Again
		boolean changed;
		do {
			changed = false;

			// Each rule
			for (GRule rule : grammar.rules()) {
				// Sum length
				InfInt upperLimit = new InfInt(1);  // upper limit on length of rule
				for (GComponent comp : rule.getGComponents()) {
					String sym = comp.getName();
					upperLimit.add(SLEN.get(sym)); // this could be optimized
				}

				if (upperLimit.isInf()) {
					continue;
				}

				// Check for New Low

				if (upperLimit.lessThan(RLEN.get(rule))) {
					RLEN.put(rule, upperLimit);
					String left = rule.leftSide();

					if (upperLimit.lessThan(SLEN.get(left))) {
						SLEN.put(left, upperLimit);
						changed = true;
						SHORT.put(left, rule);
					}
				}

			}
			
		} while (changed == true);

	}

	public static void shortestDerivation (GGrammar grammar, 
			Map<String, InfInt> SLEN, Map<GRule, InfInt> RLEN,
			Map<String, InfInt> DLEN, Map<String, GRule> PREV) {

		//Initialize
		for (String nonTerm : grammar.nonTerminals()) {
			if (grammar.isStartSymbol(nonTerm)) {
				DLEN.put(nonTerm, SLEN.get(nonTerm));
			}
			else {
				DLEN.put(nonTerm, new InfInt(true));
				PREV.put(nonTerm, null);
			}
		}

		// Try Again
		boolean changed;
		do {
			changed = false;

			// Each rule
			for (GRule rule : grammar.rules()) {
				// Compute length
				if (RLEN.get(rule).isInf()) {
					continue;
				}

				String leftSym = rule.leftSide();
				if (DLEN.get(leftSym).isInf()) {
					continue;
				}
				if (SLEN.get(leftSym).isInf()) {
					continue;
				}

				InfInt upperLimit = DLEN.get(leftSym).copy();
				upperLimit.add(RLEN.get(rule));
				upperLimit.sub(SLEN.get(leftSym));

				// Check for New low
				for (GComponent sym : rule.getGComponents()) {
					if (sym.isNonTerminal()) {
						InfInt sSL = DLEN.get(sym.getName());
						if (upperLimit.lessThan(sSL)) {

							DLEN.put(sym.getName(), upperLimit);
							PREV.put(sym.getName(), rule);
							changed = true;
						}
					}
				}
			}

		} while (changed == true);

	}

	private static void init(GGrammar grammar, 
			Map<GRule, Boolean> MARK, Map<String, Integer> ONST,
			Map<String, GRule> ONCE) {

		for (String nonTerm : grammar.nonTerminals()) {
			ONCE.put(nonTerm, grammar.ready());
			ONST.put(nonTerm, 0);
		}
		for (GRule rule : grammar.rules()) {
			MARK.put(rule, false);
		}
	}

	private static GRule handleShort(String nonTerm, GGrammar grammar,
			Map<String, GRule> SHORT, Map<GRule, Boolean> MARK, 
			Map<String, GRule> ONCE) {

		GRule rule = SHORT.get(nonTerm);
		MARK.put(rule, true);

		if (ONCE.get(nonTerm) != grammar.finished()) {
			ONCE.put(nonTerm, grammar.ready());
		}

		return rule;
	}

	private static void loadONCE(GGrammar grammar, 
			Map<GRule, Boolean> MARK, Map<String, GRule> ONCE) {

		for (GRule rule : grammar.rules()) {
			GRule leftONCE = ONCE.get(rule.leftSide());
			if (!MARK.get(rule) && (leftONCE == grammar.unsure() ||
					leftONCE == grammar.ready())) { // changed here
				ONCE.put(rule.leftSide(), rule);
				MARK.put(rule, true);
			}
		}
	}

	private static boolean processSTACK(String nt, GRule rule, 
			boolean doSentence, GGrammar grammar, Map<String, Integer> ONST,
			Stack<String> STACK) {

		boolean tmp = doSentence;
		ONST.put(nt, ONST.get(nt) - 1);

		//for (GComponent e : rule.getGComponents()) { // changed here
		for (int i = rule.getGComponents().getNumChild() - 1; i >= 0; i--) {
			GComponent e = rule.getGComponent(i);
			STACK.push(e.getName());
			if (e.isNonTerminal()) {
				ONST.put(e.getName(), ONST.get(e.getName()) + 1);
			}
		}

		boolean done = false;
		while (!done) {
			if (STACK.empty()) {
				tmp = false;
				break;
			}
			else {
				currSym = STACK.pop();
				if (grammar.terminals().contains(currSym)) {
					output.get(output.size() - 1).add(currSym);
				}
				else {
					done = true;
				}
			}
		}

		return tmp;
	}

	private static void sentenceGeneration(GGrammar grammar, 
			Map<String, GRule> SHORT, Map<String, GRule> PREV, 
			Map<GRule, Boolean> MARK, Map<String, Integer> ONST,
			Map<String, GRule> ONCE, Stack<String> STACK) {
		
		init(grammar, MARK, ONST, ONCE);

		boolean done = false;
		GRule currRule = null;
		managePrint(grammar, MARK, ONST, ONCE);

		while (!done) {
			if (ONCE.get(grammar.startSymbol()) == grammar.finished()) {
				break;
			} 

			ONST.put(grammar.startSymbol(), 1);
			currSym = grammar.startSymbol();
			boolean doSentence = true;
			output.add(new LinkedList<String>());

			while (doSentence) {
				GRule onceNonTerm = ONCE.get(currSym);

				if (grammar.isStartSymbol(currSym) &&
						onceNonTerm == grammar.finished()) {
					done = true;
					managePrint(grammar, MARK, ONST, ONCE);
					break;
				}
				else if (onceNonTerm == grammar.finished()) {
					currRule = handleShort(currSym, grammar, SHORT, MARK, ONCE);
				}
				else if (onceNonTerm.isRule()) {
					currRule = onceNonTerm;
					ONCE.put(currSym, grammar.ready());
				}
				else {
					loadONCE(grammar, MARK, ONCE);

					for (String i : grammar.nonTerminals()) {
						if (!grammar.isStartSymbol(i) && ONCE.get(i).isRule()) {
							String j = i;
							GRule k = PREV.get(j);
							while (k.isRule()) {
								j = k.leftSide();

								if (ONCE.get(j).isRule()) {
									break;
								}
								else {
									if (ONST.get(i) == 0) {
										ONCE.put(j, k);
										MARK.put(k, true);
									}
									else {
										ONCE.put(j, grammar.unsure());
									}
								}

								k = PREV.get(j);
								if (grammar.isStartSymbol(j)) {
									break;
								}
							} // while
						} // if
					} // for

					for (String n : grammar.nonTerminals()) {
						if (ONCE.get(n) == grammar.ready()) {
							ONCE.put(n, grammar.finished());
						}
					}

					if (grammar.isStartSymbol(currSym) && !ONCE.get(currSym).isRule()
						&& ONST.get(grammar.startSymbol()) == 0) {
						break;
					}
					else if (!ONCE.get(currSym).isRule()) {
						currRule = currRule = handleShort(currSym, grammar, SHORT, MARK, ONCE);
					}
					else if (ONCE.get(currSym).isRule()) {
						currRule = ONCE.get(currSym);
						ONCE.put(currSym, grammar.ready());
					}
				} // else

				doSentence = processSTACK(currSym, currRule, doSentence, grammar, ONST, STACK);
				managePrint(grammar, MARK, ONST, ONCE);

			} // while

		} // while
	}

	// print debug functions 

	private static void managePrint(GGrammar grammar,
			Map<GRule, Boolean> MARK, Map<String, Integer> ONST,
			Map<String, GRule> ONCE) {

		for (String nonTerm : grammar.nonTerminals()) {
			onceStrings.get(nonTerm).add(ONCE.get(nonTerm).toString());
			onstStrings.get(nonTerm).add(ONST.get(nonTerm).toString());
		}
		for (GRule rule : grammar.rules()) {
			markStrings.get(rule).add(MARK.get(rule).toString());
		}
	}

	private static void printSLEN (GGrammar grammar, 
			Map<String, InfInt> SLEN) {

		output.add(new LinkedList<String>());
		output.get(output.size() - 1).add("SLEN:\n");
		for (String sym : grammar.symbols()) {
			String tmpString = sym + " : " + SLEN.get(sym).getValue() + "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printSHORT (GGrammar grammar, 
			Map<String, GRule> SHORT) {

		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("SHORT:\n");
		for (String nonTerm : grammar.nonTerminals()) {
			String tmpString = nonTerm + " : " + SHORT.get(nonTerm).toString() + "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printRLEN(GGrammar grammar, 
			Map <GRule, InfInt> RLEN) {

		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("RLEN:\n");
		for (GRule rule : grammar.rules()) {
			String tmpString = rule.toString() + " : " + RLEN.get(rule).getValue() + "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printDLEN(GGrammar grammar, 
			Map<String, InfInt> DLEN) {

		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("DLEN:\n");
		for (String nonTerm : grammar.nonTerminals()) {
			String tmpString = nonTerm+ " : " + DLEN.get(nonTerm).getValue() + "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printPREV(GGrammar grammar, 
			Map<String, GRule> PREV) {

		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("PREV:\n");
		for (String nonTerm : grammar.nonTerminals()) {
			String ruleString;
			if (PREV.get(nonTerm) == null) {
				ruleString = "null";
			} else {
				ruleString = PREV.get(nonTerm).toString();
			}
			String tmpString = nonTerm + " : " + ruleString + "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printONCE(GGrammar grammar) {
		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("ONCE:\n");
		for (String nonTerm : grammar.nonTerminals()) {
			String tmpString = nonTerm + " : ";

			for (String onceString : onceStrings.get(nonTerm)) {
				tmpString += onceString + ", ";
			}

			tmpString += "\n";

			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printMARK(GGrammar grammar) {
		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("MARK:\n");
		for (GRule rule : grammar.rules()) {
			String tmpString = rule.toString() + " : ";

			for (String markString : markStrings.get(rule)) {
				tmpString += markString + ", ";
			}

			tmpString += "\n";

			output.get(output.size() - 1).add(tmpString);
		}
	}

	private static void printONST(GGrammar grammar) {
		output.add(new LinkedList<String>()); 
		output.get(output.size() - 1).add("ONST:\n");
		for (String nonTerm : grammar.nonTerminals()) {
			String tmpString = nonTerm + " : ";

			for (String onstString : onstStrings.get(nonTerm)) {
				tmpString += onstString + ", ";
			}

			tmpString += "\n";
			output.get(output.size() - 1).add(tmpString);
		}
	}

}
