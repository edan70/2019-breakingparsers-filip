/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Item {
  public final Rule rule;
  public final int dot;

  public Item(Rule rule, int dot) {
    this.rule = rule;
    this.dot = dot;
  }

  public Collection<Item> extension(Grammar grammar) {
    if (dot < rule.rhs.size()) {
      Symbol afterDot = rule.rhs.get(dot);
      return grammar.extension(afterDot);
    }
    return Collections.emptyList();
  }

  @Override public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append(rule.lhs + " =");
    for (int i = 0; i < rule.rhs.size(); ++i) {
      if (i == dot) {
        buf.append(" .");
      }
      buf.append(" ").append(rule.rhs.get(i));
    }
    if (dot == rule.rhs.size()) {
      buf.append(" .");
    }
    return buf.toString();
  }

  @Override public int hashCode() {
    return rule.hashCode() ^ dot;
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof Item) {
      Item other = (Item) obj;
      return dot == other.dot
          && rule.equals(other.rule);
    }
    return false;
  }

  public boolean canAdvance() {
    return dot < rule.rhs.size();
  }

  public Item advance() {
    if (dot >= rule.rhs.size()) {
      throw new Error("Can not advance dot in item past end.");
    }
    return new Item(rule, dot + 1);
  }


  public Item baseItem() {
    return this;
  }

  public Collection<Tuple3<ItemSet, Symbol, Action>> reduceActions(Grammar grammar, ItemSet set) {
    Collection<Tuple3<ItemSet, Symbol, Action>> actions = new ArrayList<>();
    Action action = new Reduce(rule);
    for (Symbol sym : grammar.follow(rule.lhs)) {
      actions.add(new Tuple3<>(set, sym, action));
    }
    return actions;
  }

  public Collection<Symbol> followSyms() {
    return Collections.emptyList();
  }

  public boolean related(Symbol sym) {
    return dot < rule.rhs.size() && rule.rhs.get(dot) == sym;
  }
}
