/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.extendj.neobeaver.ast.ASTNode;
import org.extendj.neobeaver.ast.GComponent;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GRule;
import static org.extendj.neobeaver.GeneratePurdomTests.shortestTerminalString;
import static org.extendj.neobeaver.GeneratePurdomTests.shortestDerivation;

public abstract class RuleMutation {

	protected GRule mutatedRule;
	private List<String> test;

	public abstract GGrammar mutate(GGrammar grammar);

	public abstract String comment();

	public List<String> getTest() {
		return test;
	}

	/**
	 * Generates a test that uses the mutatedRule 
     * @return true if it possible to generate a test for the mutated grammar
     */
	public boolean generateRuleMutationTest(GGrammar grammar) {

		// mutate one rule in the grammar
		GGrammar mutatedGrammar = this.mutate(grammar);

		// Do Purdom's two first parts.
		Map<String, InfInt> SLEN = new HashMap<>();
		Map<String, GRule> SHORT = new HashMap<>();
		Map<GRule, InfInt> RLEN = new HashMap<>();

		shortestTerminalString(mutatedGrammar, SLEN, SHORT, RLEN);

		// Returns false if a word cannot be formed from the start symbol
		if (SHORT.get(mutatedGrammar.startSymbol()) == null) {
			return false;
		}

		Map<String, InfInt> DLEN = new HashMap<>();
		Map<String, GRule> PREV = new HashMap<>();

		shortestDerivation(mutatedGrammar, SLEN, RLEN, DLEN, PREV);

		// Returns false if there is no way for lhs of the mutated rule
		// to be formed
		if (PREV.get(this.mutatedRule.leftSide()) == null) {
			return false;
		}

		// Generate shortest derivable string for mutatedRule
		List<String> shortestWordForMutRule = new LinkedList<>();
		for (GComponent comp : mutatedRule.getGComponents()) {
			String sym = comp.getName();
			
			shortestWordForMutRule.addAll(
				shortestWord(sym, SHORT, mutatedGrammar)
			);
		}

		// if the mutated rule has the start symbol on its lhs, we
		// are finished
		if (mutatedGrammar.isStartSymbol(mutatedRule.leftSide())) {
			this.test = shortestWordForMutRule;
			return true;
		}
		
		// p is the parent of to generate shortest tests, ie. PREV[m]
		GRule p;
		GRule m = mutatedRule;
		List<String> currShortestWord = new LinkedList<>();
		
		do {

			p = PREV.get(m.leftSide());

			List<String> concatWord = new LinkedList<>();
			for (GComponent comp : p.getGComponents()) {
				String sym = comp.getName();
				if (sym.equals(mutatedRule.leftSide())) {
					concatWord.addAll(shortestWordForMutRule);
				} else if (sym.equals(m.leftSide())) {
					concatWord.addAll(currShortestWord);
				} else {
					concatWord.addAll(shortestWord(sym, SHORT, mutatedGrammar));
				}
			}

			currShortestWord = concatWord;

			m = p;

		} while (!mutatedGrammar.isStartSymbol(p.leftSide()));

		this.test = currShortestWord;
		return true;

	}

	/**
	 * This method generates the shortest word derivable from a symbol
	 */
	private List<String> shortestWord(String sym, Map<String, GRule> SHORT,
			GGrammar grammar) {

		List<String> shortestDerivable = new LinkedList<>();

		if (grammar.terminals().contains(sym)) {
			shortestDerivable.add(sym);
			return shortestDerivable;
		}

		GRule shortRule = SHORT.get(sym);
		for (GComponent comp : shortRule.getGComponents()) {
			String rightSym = comp.getName();

			shortestDerivable.addAll(shortestWord(rightSym, SHORT, grammar));
		}

		return shortestDerivable;
	}

	/** 
	 * This method checks if the argument GRules are in the same position
	 * in their respective GGrammar-trees.
	 */
	protected boolean isTheSameRule(GRule r1, GRule r2) {

		// lists of GRules
		ASTNode rList1 = r1.getParent();
		ASTNode rList2 = r2.getParent();

		if (rList1.getIndexOfChild(r1) != rList2.getIndexOfChild(r2)) {
			return false;
		}

		// GProductions
		ASTNode p1 = rList1.getParent();
		ASTNode p2 = rList2.getParent();

		if (p1.getIndexOfChild(rList1) != p2.getIndexOfChild(rList2)) {
			return false;
		}

		// GProduction lists
		ASTNode pList1 = p1.getParent();
		ASTNode pList2 = p2.getParent();

		if (pList1.getIndexOfChild(p1) != pList2.getIndexOfChild(p2)) {
			return false;
		}

		// GGrammars
		ASTNode g1 = pList1.getParent();
		ASTNode g2 = pList2.getParent();

		if (g1.getIndexOfChild(pList1) != g2.getIndexOfChild(pList2)) {
			return false;
		}

		return true;
	}	

}
