/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

public class TokenNode {

	// true if a token list have ended here
	private boolean endNode;
	private Map<String, TokenNode> children;

	public TokenNode() {
		endNode = false;
		children = new HashMap<String, TokenNode>();
	}

	public TokenNode(Iterator<String> tokens) {
		children = new HashMap<String, TokenNode>();

		if (tokens.hasNext()) {
			endNode = false;
			String head = tokens.next();
			TokenNode child = new TokenNode(tokens);
			children.put(head, child);
		} else {
			endNode = true;
		}
	}

	/**
     * @return true if there were no such token sequence already
	 */
	public boolean checkNewAndAdd(Iterator<String> tokens) {
		// check if tokens end here
		if (!tokens.hasNext()) {
			// checks if another token list have ended here before
			if (endNode) {
				return false;
			} else {
				endNode = true;
				return true;
			}
		}

		String head = tokens.next();
		if (children.containsKey(head)) {
			return children.get(head).checkNewAndAdd(tokens);
		}

		// if head is not in children, add rest of test and return true
		TokenNode newChild = new TokenNode(tokens);
		children.put(head, newChild);
		return true;
	}

}
