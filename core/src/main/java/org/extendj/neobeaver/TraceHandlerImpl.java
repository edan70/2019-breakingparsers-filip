package org.extendj.neobeaver;

import org.extendj.Trace;

import java.io.IOException;

public class TraceHandlerImpl implements TraceHandler {
  Trace trace;

  public TraceHandlerImpl(String name) {
    trace = new Trace(name);
  }

  @Override
  public TraceEvent event(String name) {
    return event(name, "");
  }

  @Override
  public TraceEvent event(String name, String metadata) {
    if (name.isEmpty()) {
      throw new Error("Empty event name!");
    }
    trace.pushEvent(name, metadata);
    return new TraceEvent() {
      @Override
      public void close() {
        trace.popEvent();
      }
    };
  }

  @Override
  public void sendTo(String host, int port) throws IOException {
    trace.sendTo(host, port);
  }
}
