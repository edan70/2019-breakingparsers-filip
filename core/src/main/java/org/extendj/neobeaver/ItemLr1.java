/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ItemLr1 extends Item {
  public final Symbol follow;
  private Collection<Item> extension = null;

  public ItemLr1(Rule rule, int dot, Symbol follow) {
    super(rule, dot);
    this.follow = follow;
  }

  @Override public Collection<Item> extension(Grammar grammar) {
    if (extension != null) {
      return extension;
    }
    extension = Collections.emptyList();
    if (dot < rule.rhs.size()) {
      Symbol afterDot = rule.rhs.get(dot);
      if (!afterDot.isTerminal()) {
        List<Item> result = new ArrayList<>();
        for (Symbol x : follows(grammar)) {
          result.addAll(grammar.extension(afterDot, x));
        }
        extension = result;
      }
    }
    return extension;
  }

  private SymSet follows(Grammar grammar) {
    SymSet set = new SymSet(grammar);
    for (int i = dot + 1; i < rule.rhs.size(); ++i) {
      Symbol x = rule.rhs.get(i);
      set.or(grammar.first(x));
      if (!grammar.nullable(x)) {
        return set;
      }
    }
    set.add(follow);
    return set;
  }

  public Item advance() {
    if (dot >= rule.rhs.size()) {
      throw new Error("Can not advance dot in item past end.");
    }
    return new ItemLr1(rule, dot + 1, follow);
  }

  @Override public String toString() {
    return super.toString() + " [" + follow + "]";
  }

  @Override public Item baseItem() {
    return new Item(rule, dot);
  }

  @Override public int hashCode() {
    return rule.hashCode() ^ dot ^ follow.hashCode();
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof ItemLr1) {
      ItemLr1 other = (ItemLr1) obj;
      return dot == other.dot
          && rule.equals(other.rule)
          && follow == other.follow;
    }
    return false;
  }

  @Override public Collection<Symbol> followSyms() {
    return Collections.singleton(follow);
  }

  @Override
  public Collection<Tuple3<ItemSet, Symbol, Action>> reduceActions(Grammar grammar, ItemSet set) {
    return Collections.singleton(Tuple.of(set, follow, (Action) new Reduce(rule)));
  }

  public boolean related(Symbol sym) {
    return (dot == rule.rhs.size() && follow == sym) || super.related(sym);
  }
}
