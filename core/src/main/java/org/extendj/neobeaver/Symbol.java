/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.Collection;
import java.util.Collections;

public interface Symbol {
  Symbol EOF = new Symbol() {
    @Override public String actionName() {
      return "EOF";
    }

    @Override public boolean isTerminal() {
      return true;
    }

    @Override public int id() {
      return 0;
    }

    @Override public boolean isNamed() {
      return false;
    }

    @Override public String name() {
      return "EOF";
    }

    @Override public String toString() {
      return name();
    }

    @Override public Collection<? extends Rule> extraRules() {
      return Collections.emptyList();
    }

    @Override public Parser.SourcePosition pos() {
      return new Parser.SourcePosition(0, 0);
    }

    @Override public void setPosition(Parser.SourcePosition position) {
    }
  };

  Symbol GOAL = new Symbol() {
    @Override public String actionName() {
      return "GOAL";
    }

    @Override public boolean isTerminal() {
      return false;
    }

    @Override public int id() {
      return 1;
    }

    @Override public boolean isNamed() {
      return false;
    }

    @Override public String name() {
      return "GOAL";
    }

    @Override public String toString() {
      return name();
    }

    @Override public Collection<? extends Rule> extraRules() {
      return Collections.emptyList();
    }

    @Override public Parser.SourcePosition pos() {
      return new Parser.SourcePosition(0, 0);
    }

    @Override public void setPosition(Parser.SourcePosition position) {
    }
  };

  String actionName();

  boolean isTerminal();

  int id();

  boolean isNamed();

  String name();

  Collection<? extends Rule> extraRules();

  /** Get the source position where this symbol is declared. */
  Parser.SourcePosition pos();

  void setPosition(Parser.SourcePosition position);
}
