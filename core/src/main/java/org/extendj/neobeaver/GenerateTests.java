/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.List;

import org.extendj.neobeaver.ast.GGrammar;
import static org.extendj.neobeaver.GenRuleMutations.generateRuleMutations;
import static org.extendj.neobeaver.GeneratePurdomTests.generatePurdomTests;
import static org.extendj.neobeaver.GenWordMutations.generateWordMutations;
import org.extendj.neobeaver.RuleMutation;
import org.extendj.neobeaver.TokenNode;
import static org.extendj.neobeaver.Util.deleteDirectory;
import static org.extendj.neobeaver.Util.parse;
import org.extendj.neobeaver.WordMutation;

public class GenerateTests {

    private static final String POS = "pos_test_";
    private static final String NEG = "neg_test_";
    private static final String END = ".txt";

    private static int nPosTests;
    private static int nNegTests;

    private static TokenNode posTree;
    private static TokenNode negTree;

    public static void generateTests(String filePath, Path outDir) 
            throws Exception {

    	File inputFile = new File(filePath);
    	Path inPath = inputFile.toPath();

        String testDirName = "Generated Tests for " + inPath.getFileName();
        Path testPath = outDir.resolve(testDirName);
        File testDir = testPath.toFile();
    	if (testDir.isDirectory()) {
      		// Clean old test files.
      		deleteDirectory(testPath);
    	}
        testDir.mkdir();

        GGrammar grammar = (GGrammar) parse(inPath);
    	boolean isSimpliC = inPath.endsWith("parser.beaver");
        nPosTests = 0;
        nNegTests = 0;
        posTree = new TokenNode();
        negTree = new TokenNode();

        // gen purdom tests
        List<List<String>> purdomTests = generatePurdomTests(grammar);

        // print purdom tests
        printPosTests(purdomTests, isSimpliC, testDir);

        // print word mutations of purdom
        printWordMutations(purdomTests, grammar, isSimpliC, testDir);

        // print rule mutations of purdom
        printRuleMutations(grammar, isSimpliC, testDir);

    }

    private static void printPosTests(List<List<String>> tests,
            boolean isSimpliC, File testDir) throws Exception {

        for (List<String> test : tests) {
            if (posTree.checkNewAndAdd(test.iterator())) {
                String outputFileName = POS + nPosTests + END;

                printTest(outputFileName, test, isSimpliC, testDir);
                nPosTests++;
            }
        }
    }

    private static void printWordMutations(List<List<String>> posTests, 
            GGrammar grammar, boolean isSimpliC, File testDir) 
            throws Exception {

        for (List<String> test : posTests) {
            List<WordMutation> mutations; 
            mutations = generateWordMutations(grammar, test);

            for (WordMutation m : mutations) {
                List<String> mutatedTest = m.mutate(test);
                if (negTree.checkNewAndAdd(mutatedTest.iterator())) {
                    String outputFileName = NEG + nNegTests + END;

                    printTest(outputFileName, mutatedTest, isSimpliC, testDir);
                    nNegTests++;
                }
            }
        }
    }

    private static void printRuleMutations(GGrammar grammar, boolean isSimpliC, 
            File testDir) throws Exception {

        List<RuleMutation> ruleMutations = generateRuleMutations(grammar);
        for (RuleMutation mutation : ruleMutations) {
            List<String> mutatedTest;
            if (mutation.generateRuleMutationTest(grammar)) {
                mutatedTest = mutation.getTest();
            }
            else {
                continue;
            }

            if (negTree.checkNewAndAdd(mutatedTest.iterator())) {
                String outputFileName = NEG + nNegTests + END;

                printTest(outputFileName, mutatedTest, isSimpliC, testDir);
                nNegTests++;
            }
        }
    }

    private static void printTest(String fileName, List<String> tokens, 
            boolean isSimpliC, File testDir) throws Exception {

    	File file = new File(testDir, fileName);
    	PrintStream out = new PrintStream(new FileOutputStream(file));

    	for (String token : tokens) {
    		if (isSimpliC) {
    			out.print(SimpliCPrettyPrinting.prettyPrint(token));
    		}    			
    		else {
    			out.print(token + " ");
    		}
    	}

    	if (isSimpliC) {
    		SimpliCPrettyPrinting.resetIndent();
    	}

    	out.close();
    }

}
