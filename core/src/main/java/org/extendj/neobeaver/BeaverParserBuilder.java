package org.extendj.neobeaver;

import org.extendj.neobeaver.ast.ASTNode;
import org.extendj.neobeaver.ast.ClassDecl;
import org.extendj.neobeaver.ast.EmbedCode;
import org.extendj.neobeaver.ast.GComponent;
import org.extendj.neobeaver.ast.GDecl;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GPrecedence;
import org.extendj.neobeaver.ast.GProduction;
import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GSym;
import org.extendj.neobeaver.ast.GVisitor;
import org.extendj.neobeaver.ast.Goal;
import org.extendj.neobeaver.ast.HeaderCode;
import org.extendj.neobeaver.ast.LeftAssoc;
import org.extendj.neobeaver.ast.NonAssoc;
import org.extendj.neobeaver.ast.PackageDecl;
import org.extendj.neobeaver.ast.RightAssoc;
import org.extendj.neobeaver.ast.Terminals;
import org.extendj.neobeaver.ast.TypeOf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

// TODO: add source positions to warning and errors.
/**
 * Builds the production type map and determines the goal symbol.
 */
public class BeaverParserBuilder {
  private Symbol goalSym;
  private List<Rule> rules;
  private Stack<GDecl> stack;
  private Map<String, String> typeMap;
  private SymbolCache cache;
  private Set<String> left;
  private Set<String> right;
  private Set<String> nonassoc;
  private Set<String> assoc;
  private Map<String, Integer> precedence;
  private Set<Symbol> terminals;
  private Set<String> terminalNames;
  private String packageName = "";
  private boolean alreadyDefinedClassName = false;
  private String className = "Parser";
  private String header = "";
  private String embed = "";

  private int nextPrecedence = 1;
  private final ProblemHandler problems;

  public BeaverParserBuilder(ProblemHandler problems) {
    this.problems = problems;
    goalSym = Symbol.EOF;
    rules = new ArrayList<>();
    stack = new Stack<>();
    typeMap = new HashMap<>();
    cache = new SymbolCache();
    left = new HashSet<>();
    right = new HashSet<>();
    nonassoc = new HashSet<>();
    assoc = new HashSet<>();
    precedence = new HashMap<>();
    terminals = new HashSet<>();
    terminalNames = new HashSet<>();
  }

  public Grammar build(TraceHandler trace) {
    return GrammarBuilder.build(trace, rules, terminals, goalSym, embed,
        header, className, packageName,
        left, right, nonassoc, precedence);
  }

  public void defaultParserName(String defaultParserName) {
    className = defaultParserName;
  }

  public void grammarSpec(GGrammar grammarSpec, TraceHandler trace) {
    try (TraceEvent ignored1 = trace.event("parse")) {
      try (TraceEvent ignored2 = trace.event("typeMap")) {
        // First, build the production type map and determine the goal symbol.
        GVisitor visitor = new FirstPass();
        visitor.visit(grammarSpec);
      }

      try (TraceEvent ignored2 = trace.event("productions")) {
        // Now we build all productions.
        GVisitor visitor = new SecondPass();
        visitor.visit(grammarSpec);
      }
    }
  }

  /**
   * The first pass gathers all NTA names and type declarations.
   */
  class FirstPass implements GVisitor {
    @Override
    public void visit(GProduction prod) {
      // TODO(joqvist): handle conflicting production types for the same NTA.
      String name = prod.getName();
      if (prod.hasType()) {
        addType(prod, name, prod.getType().getName());
      }
      Symbol lhs;
      if (cache.contains(name)) {
        lhs = cache.get(name);
      } else {
        lhs = cache.nta(name);
        lhs.setPosition(prod.getPosition());
      }
      lhs.setPosition(prod.getPosition());
      if (goalSym == Symbol.EOF) {
        goalSym = lhs;
      }
    }

    @Override
    public void visit(GGrammar grammar) {
      for (GDecl decl : grammar.getGDeclList()) {
        decl.accept(this);
      }
    }

    @Override
    public void visit(GRule prod) {

    }

    @Override
    public void visit(GPrecedence prod) {

    }

    @Override
    public void visit(HeaderCode prod) {
      header += prod.getCode();
    }

    @Override
    public void visit(EmbedCode prod) {
      embed += prod.getCode();
    }

    @Override
    public void visit(Terminals prod) {
    }

    @Override
    public void visit(TypeOf nta) {
      // TODO: handle type declaration errors.
      addType(nta, nta.getName(), nta.getType());
    }

    @Override
    public void visit(Goal nta) {
      if (goalSym == Symbol.EOF) {
        goalSym = cache.nta(nta.getName());
        goalSym.setPosition(nta.getPosition());
      } else {
        problems.warnf("%s multiple goal symbols specified.", nta.getPosition());
      }
    }

    @Override
    public void visit(ClassDecl prod) {

    }

    @Override
    public void visit(PackageDecl prod) {

    }

    @Override
    public void visit(LeftAssoc assoc) {

    }

    @Override
    public void visit(RightAssoc assoc) {

    }

    @Override
    public void visit(NonAssoc assoc) {

    }

    private void addType(ASTNode position, String name, String type) {
      if (typeMap.containsKey(name) && !typeMap.get(name).equals(type)) {
        problems.warnf("%s: conflicting type declarations for %s: "
                + "new type %s is not same as previous type %s",
            position.getPosition(),
            name, type, typeMap.get(name));
      }
      typeMap.put(name, type);
    }
  }

  /**
   * The second pass builds production rules.
   */
  class SecondPass implements GVisitor {

    @Override
    public void visit(GGrammar grammar) {
      for (GDecl decl : grammar.getGDeclList()) {
        decl.accept(this);
      }
    }

    @Override
    public void visit(GProduction prod) {
      String name = prod.getName();
      String lhsType;
      if (typeMap.containsKey(name)) {
        lhsType = typeMap.get(name);
      } else {
        lhsType = "Symbol";
      }
      Symbol lhs;
      if (cache.contains(name)) {
        lhs = cache.get(name);
      } else {
        lhs = cache.nta(name);
        lhs.setPosition(prod.getPosition());
      }
      for (GRule rule : prod.getGRuleList()) {
        List<Symbol> rhs = new ArrayList<>();
        List<String> names = new ArrayList<>();
        parseComponents(rule, rhs, names);
        String precedenceSym = rule.hasPrecedence() ? rule.getPrecedence().getName() : "";
        if (rule.hasAction()) {
          // This rule has a semantic action.
          String action = rule.getAction().getCode();
          rules.add(new Rule(-1, lhs, rhs, action, names, lhsType, precedenceSym));
        } else {
          // No semantic action.
          rules.add(new Rule(-1, lhs, rhs, "", names, lhsType, precedenceSym));
        }
      }
    }

    @Override
    public void visit(GRule rule) {

    }

    @Override
    public void visit(GPrecedence precedence) {

    }

    @Override
    public void visit(HeaderCode code) {

    }

    @Override
    public void visit(EmbedCode code) {

    }

    @Override
    public void visit(Terminals list) {
      for (GSym t : list.getSymList()) {
        String name = t.getName();
        if (terminalNames.contains(name)) {
          problems.warnf("duplicate terminal token declared: %s", name);
        } else {
          Symbol term = cache.terminal(name);
          term.setPosition(t.getPosition());
          terminals.add(term);
          terminalNames.add(name);
        }
      }
    }

    @Override
    public void visit(TypeOf typeOf) {

    }

    @Override
    public void visit(Goal goal) {

    }

    @Override
    public void visit(ClassDecl classDecl) {
      if (alreadyDefinedClassName) {
        problems.warn("redeclaring parser class name!"); // TODO: add source position to warning.
      }
      alreadyDefinedClassName = true;
      className = classDecl.getName();
      if (className.isEmpty()) {
        problems.warn("empty class name!"); // TODO: add source position to warning.
      }
    }

    @Override
    public void visit(PackageDecl packageDecl) {
      if (!packageName.isEmpty()) {
        problems.warn("redeclaring package name!"); // TODO: add source position to warning.
      }
      packageName = packageDecl.getName();
    }

    @Override
    public void visit(LeftAssoc assoc) {
      parseAssocRules(assoc.getSymList(), left);
    }

    @Override
    public void visit(RightAssoc assoc) {
      parseAssocRules(assoc.getSymList(), right);
    }

    @Override
    public void visit(NonAssoc assoc) {
      parseAssocRules(assoc.getSymList(), nonassoc);
    }
  }

  private void parseAssocRules(org.extendj.neobeaver.ast.List<GSym> symList, Set<String> assocSet) {
    for (GSym sym : symList) {
      String name = sym.getName();
      if (assoc.contains(name)) {
        problems.warnf("associativity already specified for %s", name);
      } else {
        precedence.put(name, nextPrecedence++);
        assoc.add(name);
        assocSet.add(name);
      }
    }
  }

  private void parseComponents(GRule rule, List<Symbol> rhs, List<String> names) {
    Set<String> usedNames = new HashSet<>();
    for (GComponent com : rule.getGComponentList()) {
      Symbol sym = parseSymbol(com);
      String quantifier = com.getQuantifier();
      switch (quantifier) {
        case "":
          rhs.add(sym);
          break;
        case "*":
          rhs.add(ListComponent.buildOptional(sym, cache));
          break;
        case "+":
          rhs.add(ListComponent.build(sym, cache));
          break;
        case "?":
          rhs.add(OptionalComponent.build(sym, cache));
          break;
        default:
          throw new Error("Unknown quantifier: " + quantifier);
      }
      String actionName;
      if (com.hasActionName()) {
        actionName = com.getActionName().getName();
      } else {
        // Assign automatic name.
        actionName = sym.name();
        int suffix = 2;
        // Rename the symbol until we find a name that is not used.
        while (usedNames.contains(actionName)) {
          actionName = sym.name() + suffix;
          suffix += 1;
        }
      }
      if (usedNames.contains(actionName)) {
        problems.errorf(
            "%s: can not reuse component name \"%s\" in this production!",
            rule.getPosition(), actionName);
      } else {
        usedNames.add(actionName);
        names.add(actionName);
      }
    }
  }

  /**
   * @return a tuple containing the symbol and action name of a production component.
   */
  private Symbol parseSymbol(GComponent sym) {
    String name = sym.getName();
    if (cache.contains(name)) {
      return cache.get(name);
    } else {
      problems.warnf("%s: implicit declaration of terminal symbol [%s].", sym.getPosition(), name);
      Symbol component = cache.terminal(name);
      component.setPosition(sym.getPosition());
      return component;
    }
  }

}
