/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.DeflaterOutputStream;

public class Grammar {
  public static final boolean VERBOSE = false;

  private final List<Rule> originalRules;
  public final List<Rule> rules;
  public final Symbol goalSym;
  public final Set<Symbol> syms;
  public final Map<Integer, Symbol> symIndex;
  private final BitSet nullable;
  private final Map<Symbol, SymSet> first;
  private final Map<Symbol, Set<Symbol>> follow;
  private final Map<Symbol, Set<Item>> extension;
  private final ExtensionBuilderLr1 extensionBuilder;
  private final ItemLr1 goalItem;
  private final String embed;
  private final String header;
  public final String className;
  public final String packageName;
  private final Map<Set<Item>, ItemSet> setCache = new HashMap<>();
  private final Map<ItemSet, ItemSet> extensionMap = new HashMap<>();
  private final Set<String> left;
  private final Set<String> right;
  private final Set<String> nonassoc;
  private final Map<String, Integer> precedence;
  private int nextItemId = 1;
  private boolean lalr = false;

  public Grammar(List<Rule> originalRules, List<Rule> rules, Symbol goalSym, Rule goalRule, Set<Symbol> syms,
      BitSet nullable, Map<Symbol, Set<Symbol>> first,
      Map<Symbol, Set<Symbol>> follow, Map<Symbol, Set<Item>> extension,
      ExtensionBuilderLr1 extensionBuilder, String embed, String header, String className, String packageName,
      Set<String> left, Set<String> right, Set<String> nonassoc, Map<String, Integer> precedence) {
    this.originalRules = originalRules;
    this.rules = rules;
    this.goalSym = goalSym;
    this.syms = syms;
    this.nullable = nullable;
    this.first = new HashMap<>();
    this.follow = follow;
    this.extension = extension;
    this.extensionBuilder = extensionBuilder;
    this.goalItem = new ItemLr1(goalRule, 0, Symbol.EOF);
    this.embed = embed;
    this.header = header;
    this.className = className;
    this.packageName = packageName;
    this.left = left;
    this.right = right;
    this.nonassoc = nonassoc;
    this.precedence = precedence;
    symIndex = new HashMap<>(syms.size());
    for (Symbol sym : syms) {
      if (symIndex.containsKey(sym.id())) {
        throw new Error("Duplicate symbol ID: " + sym.id());
      }
      symIndex.put(sym.id(), sym);
    }
    for (Map.Entry<Symbol, Set<Symbol>> entry : first.entrySet()) {
      this.first.put(entry.getKey(), SymSet.of(entry.getValue(), this));
    }
  }

  public static String typeOf(Symbol symbol, Collection<Rule> rules) {
    for (Rule rule : rules) {
      if (rule.lhs == symbol) {
        return rule.type();
      }
    }
    return "Symbol";
    //throw new Error(String.format("Symbol %s not found in grammar rules.", symbol));
  }

  /**
   * Transform grammar rules to canonical form.
   */
  public static Collection<Rule> canonicalRules(Collection<Rule> rules) {
    Set<Rule> result = new HashSet<>();
    for (Rule rule : rules) {
      result.addAll(rule.canonical());
      result.addAll(rule.extraRules());
    }
    return result;
  }

  public MyParser buildParser(ProblemHandler problems, TraceHandler trace) {
    ItemSet goal = addItemSet(goalItem);
    Set<ItemSet> exactCores = new HashSet<>();
    Set<ItemSet> mergedSets = new HashSet<>();
    Map<ItemSet, ItemSet> baseCores = new LinkedHashMap<>();
    List<ItemSet> newSets = new ArrayList<>();
    newSets.add(goal);
    baseCores.put(maybeLalrCore(goal.core()), goal);
    List<Tuple3<ItemSet, ItemSet, Symbol>> transitions = new ArrayList<>();
    try (TraceEvent ignored = trace.event("transitions")) {
      do {
        List<ItemSet> next = new ArrayList<>();
        for (ItemSet set : newSets) {
          try (TraceEvent ignored2 = trace.event("follow cores")) {
            Collection<Tuple<Symbol, ItemSet>> followCores = set.followCores();
            try (TraceEvent ignored3 = trace.event("extend cores")) {
              for (Tuple<Symbol, ItemSet> tuple : followCores) {
                Symbol sym = tuple.first;
                ItemSet follow = tuple.second;
                ItemSet baseCore;
                try (TraceEvent ignored4 = trace.event("maybeLalrCore")) {
                  baseCore = maybeLalrCore(follow);
                }
                try (TraceEvent ignored4 = trace.event("add transition")) {
                  transitions.add(new Tuple3<>(maybeLalrCore(set.core()), baseCore, sym));
                }
                if (baseCores.containsKey(baseCore)) {
                  if (!mergedSets.contains(follow)) {
                    ItemSet existing = baseCores.get(baseCore);
                    if (follow != existing.core()) {
                      try (TraceEvent ignored4 = trace.event("merge")) {
                        ItemSet merged = existing.merge(follow.items, extension(follow.items), this);
                        if (!exactCores.contains(merged.core())) {
                          exactCores.add(merged.core());
                          next.add(merged);
                          baseCores.put(baseCore, merged);
                        } else {
                          //System.out.println("duplicate");
                        }
                      }
                    }
                  }
                } else {
                  try (TraceEvent ignored4 = trace.event("add item set")) {
                    ItemSet extended = new ItemSet(-1, follow, extension(follow.items));
                    exactCores.add(follow);
                    next.add(extended);
                    baseCores.put(baseCore, extended);
                  }
                }
                mergedSets.add(follow);
              }
            }
          }
        }
        newSets = new ArrayList<>(next);
      } while (!newSets.isEmpty());
    }
    /*for (Map.Entry<ItemSet, ItemSet> entry : cores.entrySet()) {
      ItemSet core = entry.getKey();
      ItemSet ext = entry.getValue();
      System.out.printf("core : %s%nextension : %s%n", core, ext);
    }*/
    List<Tuple3<ItemSet, Symbol, Action>> actions = new ArrayList<>();
    List<ItemSet> itemSets;
    try (TraceEvent ignored = trace.event("enumerate")) {
      itemSets = enumerateItems(baseCores.values());
    }
    Map<ItemSet, ItemSet> coreMap;
    try (TraceEvent ignored = trace.event("coreMap")) {
      coreMap = new HashMap<>();
      for (ItemSet set : itemSets) {
        coreMap.put(maybeLalrCore(set.core()), set);
      }
    }
    ItemSet renamedGoal = coreMap.get(maybeLalrCore(goal.core()));
    //System.out.println(renamedGoal);
    actions.add(new Tuple3<>(renamedGoal, Symbol.GOAL, Action.ACCEPT));
    TransitionTable table;
    try (TraceEvent ignored = trace.event("TransitionTables.build")) {
      table = TransitionTable.build(this, problems, syms,
          itemSets, renamedGoal, coreMap, transitions, actions);
    }
    return new MyParser(this, table, renamedGoal, itemSets);
  }

  /**
   * @return the precedence level for the given token. Lower values
   * are higher precedence.
   */
  public int precedence(String token) {
    if (precedence.containsKey(token)) {
      return precedence.get(token);
    } else {
      return Integer.MAX_VALUE;
    }
  }

  /**
   * @return {@code true} if the token is left-associative
   */
  public boolean left(String token) {
    return left.contains(token);
  }

  /**
   * @return {@code true} if the token is right-associative
   */
  public boolean right(String token) {
    return right.contains(token);
  }

  /**
   * @return {@code true} if the token is non-associative
   */
  public boolean nonassoc(String token) {
    return nonassoc.contains(token);
  }

  private static List<ItemSet> enumerateItems(Collection<ItemSet> items) {
    int id = 1; // IDs should start at 1 for the beaver parser generation.
    List<ItemSet> result = new ArrayList<>();
    for (ItemSet item : items) {
      result.add(new ItemSet(id, item.core, item.extension));
      id += 1;
    }
    return result;
  }

  private ItemSet maybeLalrCore(ItemSet core) {
    //if (lalr) {
      return core.baseCore();
    //}
  }

  private ItemSet addItemSet(Item item) {
    return addItemSet(Collections.singleton(item));
  }

  private ItemSet addItemSet(Set<Item> items) {
    // TODO represent core sets with bitsets.
    if (setCache.containsKey(items)) {
      return setCache.get(items);
    } else {
      int id = nextItemId;
      nextItemId += 1;
      ItemSet core = new ItemSet(id, items);
      setCache.put(items, core);
      ItemSet extended = new ItemSet(id, core, extension(items));
      extensionMap.put(core, extended);
      return extended;
    }
  }

  private Set<Item> extension(Set<Item> items) {
    Set<Item> set = new HashSet<>();
    for (Item item : items) {
      set.addAll(item.extension(this));
    }
    return set;
  }

  public SymSet first(Symbol sym) {
    return first.get(sym);
  }

  public boolean nullable(Symbol sym) {
    return nullable.get(sym.id());
  }

  public Collection<Item> extension(Symbol sym) {
    throw new Error("Non-LR1 extensions not implemented.");
  }

  public Collection<? extends Item> extension(Symbol a, Symbol b) {
    return extensionBuilder.extension(a, b);
  }

  public Collection<Symbol> follow(Symbol lhs) {
    return follow.get(lhs);
  }

  private static Tuple<Integer, Integer> range(Set<Integer> sortedSyms) {
    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    for (Integer value : sortedSyms) {
      if (value < min) {
        min = value;
      }
      if (value > max) {
        max = value;
      }
    }
    return new Tuple<>(min, max);
  }

  private static boolean allSame(Collection<Integer> values) {
    /*if (values.size() <= 1) {
      return true;
    }
    Iterator<Integer> iter = values.iterator();
    int prev = iter.next();
    while (iter.hasNext()) {
      if (iter.next() != prev) {
        return false;
      }
    }
    return true;*/
    return false;
  }

  public void printTables(MyParser parser) {
    TransitionTable transitions = parser.transitions;
    List<ItemSet> itemSets = parser.itemSets;

    // Sort symbols by number of occurrences in actions.
    Map<ItemSet, Set<Symbol>> usedSyms = new LinkedHashMap<>();
    for (ItemSet state : itemSets) {
      Set<Symbol> syms = new HashSet<>();
      syms.addAll(transitions.actions.get(state).keySet());
      syms.addAll(transitions.map.get(state).keySet());
      usedSyms.put(state, syms);
    }
    Map<Symbol, Integer> occurrences = new HashMap<>();
    for (Set<Symbol> symbols : usedSyms.values()) {
      for (Symbol sym : symbols) {
        if (occurrences.containsKey(sym)) {
          occurrences.put(sym, occurrences.get(sym) + 1);
        } else {
          occurrences.put(sym, 0);
        }
      }
    }
    occurrences.put(Symbol.EOF, Integer.MAX_VALUE); // EOF should be sorted first.

    List<Map.Entry<Symbol, Integer>> sorted = new ArrayList<>(occurrences.entrySet());
    Collections.sort(sorted, new Comparator<java.util.Map.Entry<Symbol, Integer>>() {
      @Override public int compare(Map.Entry<Symbol, Integer> o1, Map.Entry<Symbol, Integer> o2) {
        return o2.getValue().compareTo(o1.getValue());
      }
    });
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Map.Entry<Symbol, Integer> entry : sorted) {
      Symbol sym = entry.getKey();
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }
    Map<Symbol, Integer> sym2id = new HashMap<>();
    Map<Integer, Symbol> id2sym = new HashMap<>();
    int nextSymId = 0;
    for (Symbol sym : terminals) {
      sym2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }
    for (Symbol sym : nonterminals) {
      sym2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }

    for (ItemSet state : itemSets) {
      System.out.format("S%d%n", state.id());
      Map<Symbol, Action> setActions = transitions.actions.get(state);
      Map<Symbol, ItemSet> trans = transitions.map.get(state);
      for (Symbol tok : terminals) {
        Action action = setActions.get(tok);
        if (action instanceof Reduce) {
          Reduce reduce = (Reduce) action;
          System.out.format("    %s: REDUCE %s%n", tok, reduce.rule);
        } else if (action instanceof Shift) {
          Shift shift = (Shift) action;
          System.out.format("    %s: SHIFT; goto S%d%n", tok, shift.next.id());
        }
      }
      for (Symbol sym : nonterminals) {
        ItemSet next = trans.get(sym);
        if (next != null) {
          System.out.format("    %s: SHIFT; goto S%d%n", sym, next.id());
        } else {
          Action action = setActions.get(sym);
          if (action == Action.ACCEPT) {
            System.out.format("    %s: ACCEPT%n", sym);
          }
        }
      }
    }
  }

  public void printBeaverSpec(PrintStream out, MyParser parser) throws IOException {
    if (!packageName.isEmpty()) {
      out.format("%%package \"%s\";%n", packageName);
    }
    if (!header.isEmpty()) {
      // Emit embedded code.
      out.format("%%header {%n%s%n};%n", header);
    }
    if (!embed.isEmpty()) {
      // Emit embedded code.
      out.format("%%embed {%n%s%n};%n", embed);
    }

    TransitionTable transitions = parser.transitions;
    List<ItemSet> itemSets = parser.itemSets;
    Set<Symbol> usedSyms = new HashSet<>();
    for (ItemSet state : itemSets) {
      usedSyms.addAll(transitions.actions.get(state).keySet());
      usedSyms.addAll(transitions.map.get(state).keySet());
    }
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Symbol sym : usedSyms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }

    for (Symbol sym : terminals) {
      out.format("%%terminals %s;%n", sym);
    }

    for (Symbol sym : nonterminals) {
      String type = typeOf(sym, rules);
      if (!type.equals("Symbol")) {
        out.format("%%typeof %s = \"%s\";%n", sym.name(), type);
      }
    }

    out.println();
    out.format("%%goal %s;%n", goalSym.name());
    out.println();

    for (Symbol sym : nonterminals) {
      out.format("%s =%n", sym.name());
      boolean first = true;
      for (Rule rule : originalRules) {
        if (rule.lhs == sym) {
          if (first) {
            first = false;
            out.print("    ");
          } else {
            out.print("  | ");
          }
          for (int i = 0; i < rule.rhs.size(); ++i) {
            if (i > 0) {
              out.print(" ");
            }
            Symbol rhsSym = rule.rhs.get(i);
            String symName;
            if (i < rule.names.size()) {
              symName = rule.names.get(i);
            } else {
              symName = rhsSym.name();
            }
            out.format("%s.%s", rhsSym.name(), symName);
          }
          if (!rule.action.isEmpty()) {
            out.format(" {: %s :}", rule.action);
          }
          out.println();
        }
      }
      out.println("  ;");
    }
  }

  public void printBeaverTestSpec(PrintStream out, MyParser parser) throws IOException {
    if (!packageName.isEmpty()) {
      out.format("%%package \"%s\";%n", packageName);
    }
    if (!header.isEmpty()) {
      // Emit embedded code.
      out.format("%%header {%n%s%n};%n", header);
    }
    if (!embed.isEmpty()) {
      // Emit embedded code.
      out.format("%%embed {%n%s%n};%n", embed);
    }

    TransitionTable transitions = parser.transitions;
    List<ItemSet> itemSets = parser.itemSets;
    Set<Symbol> usedSyms = new HashSet<>();
    for (ItemSet state : itemSets) {
      usedSyms.addAll(transitions.actions.get(state).keySet());
      usedSyms.addAll(transitions.map.get(state).keySet());
    }
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Symbol sym : usedSyms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }

    for (Symbol sym : terminals) {
      out.format("%%terminals %s;%n", sym);
    }

    for (Symbol sym : nonterminals) {
      String type = typeOf(sym, rules);
      if (!type.equals("Symbol")) {
        out.format("%%typeof %s = \"%s\";%n", sym.name(), type);
      }
    }

    out.println();
    out.format("%%goal %s;%n", goalSym.name());
    out.println();

    for (Symbol sym : nonterminals) {
      out.format("%s =%n", sym.name());
      boolean first = true;
      for (Rule rule : originalRules) {
        if (rule.lhs == sym) {
          if (first) {
            first = false;
            out.print("    ");
          } else {
            out.print("  | ");
          }
          for (int i = 0; i < rule.rhs.size(); ++i) {
            if (i > 0) {
              out.print(" ");
            }
            Symbol rhsSym = rule.rhs.get(i);
            String symName;
            if (i < rule.names.size()) {
              symName = rule.names.get(i);
            } else {
              symName = rhsSym.name();
            }
            out.format("%s.%s", rhsSym.name(), symName);
          }
          out.format(" {: return new Nonterminal(\"%s\"", sym.name());
          for (int i = 0; i < rule.rhs.size(); ++i) {
            Symbol rhsSym = rule.rhs.get(i);
            String symName;
            if (i < rule.names.size()) {
              symName = rule.names.get(i);
            } else {
              symName = rhsSym.name();
            }
            out.format(", %s", symName);
          }
          out.println("); :}");
        }
      }
      out.println("  ;");
    }
  }

  /** Generate a non-Beaver parser. */
  public void printParser(PrintStream out, MyParser parser, boolean debug) {
    TransitionTable transitions = parser.transitions;
    List<ItemSet> itemSets = parser.itemSets;

    out.println("// Parser generated by NeoBeaver.");
    if (!packageName.isEmpty()) {
      out.format("package %s;%n", packageName);
    }
    if (!header.isEmpty()) {
      // Emit embedded code.
      out.println(header);
    }

    out.println("import java.io.IOException;");
    out.println("import java.util.Stack;");
    out.println();
    out.format("public class %s {%n", className);
    out.println();
    out.println("  /** Interface implemented by scanners that this parser can use. */");
    out.println("  public interface TokenScanner {");
    out.println("    Token nextToken() throws IOException;");
    out.println("  }");
    out.println();
    out.println("  public static class SourcePosition {");
    out.println("    int line, column;");
    out.println();
    out.println("    public SourcePosition(int line, int column) {");
    out.println("      this.line = line;");
    out.println("      this.column = column;");
    out.println("    }");
    out.println();
    out.println("    @Override public String toString() {");
    out.println("      return \"\" + line + \":\" + column;");
    out.println("    }");
    out.println("  }");

    if (!embed.isEmpty()) {
      // Emit embedded code.
      out.format("%s%n", embed);
    }

    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Symbol sym : syms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }
    Map<Symbol, Integer> tok2id = new HashMap<>();
    Map<Symbol, Integer> sym2id = new HashMap<>();
    Map<Integer, Symbol> id2sym = new HashMap<>();
    int nextSymId = 0;
    for (Symbol sym : terminals) {
      tok2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }
    nextSymId = 0;
    for (Symbol sym : nonterminals) {
      sym2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }

    // Print token table.
    out.println("  public interface Tokens {");
    for (Symbol sym : terminals) {
      int tokId = tok2id.get(sym);
      if (sym.isNamed()) {
        out.format("    int %s = %d;%n", sym.name(), tokId);
      } else if (sym == Symbol.EOF) {
        out.format("    int EOF = %d;%n", tokId);
      } else if (sym.isTerminal()) {
        out.format("    int TOKEN_%d = %d;%n", tokId, tokId);
      } else {
        throw new Error("Can't generate parser with unnamed nonterminal!");
      }
    }
    out.println("  }");
    out.println();

    out.println("  public static class SyntaxError extends Exception {");
    out.println("    private Symbol sym;");
    out.println();
    out.println("    public SyntaxError(Symbol sym) {");
    out.println("      this.sym = sym;");
    out.println("    }");
    out.println();
    out.println("    @Override");
    out.println("    public String getMessage() {");
    out.println("      return String.format(\"syntax error at token \\\"%s\\\" at %s\", sym, sym.getPosition());");
    out.println("    }");
    out.println("  }");
    out.println();
    out.println("  void syntaxError(Symbol sym) throws SyntaxError {");
    out.println("    throw new SyntaxError(sym);");
    out.println("  }");
    out.println();
    out.println("  static Token nextToken(TokenScanner in) throws IOException {");
    out.println("    return in.nextToken();");
    out.println("  }");
    out.println();
    out.println("  private static final boolean DEBUG = System.getProperty(\"debug\", \"\").equalsIgnoreCase(\"true\");");
    out.println();
    out.println("  public Symbol parse(TokenScanner in) throws IOException, SyntaxError {");
    out.println("    Stack<Integer> stateStack = new Stack<Integer>();");
    out.println("    Stack<Symbol> stack = new Stack<Symbol>();");
    out.println("    int state = 1;");
    out.println("    Token next = nextToken(in);");
    out.println("    while (state != -1) {");
    out.println("      if (state == 0) throw new Error(\"parse error\");"); // TODO: improve error messages.

    out.println("      if (DEBUG) {");
    out.println("        System.out.print(\"state:\");");
    out.println("        for (int st : stateStack) {");
    out.println("          System.out.print(\" \" + st);");
    out.println("        }");
    out.println("        System.out.println(\" \" + state);");
    out.println("      }");
    out.println("      switch (state) {");

    int nextId = 0;
    Map<Reduce, Integer> reduceActions = new HashMap<>();

    for (ItemSet set : itemSets) {
      int state = set.id();
      out.format("        case %d:%n", state);
      Map<Symbol, Action> setActions = transitions.actions.get(set);
      out.println("          if (DEBUG) System.out.format(\"[%s] \", next);");
      out.println("          switch (next.getId()) {");
      for (Symbol tok : terminals) {
        out.format("            case %d: {%n", tok2id.get(tok));
        if (setActions.containsKey(tok)) {
          Action action = setActions.get(tok);
          if (action instanceof Reduce) {
            Reduce reduce = (Reduce) action;
            Rule rule = reduce.rule;
            if (!(reduceActions.containsKey(reduce))) {
               reduceActions.put(reduce, nextId);
               nextId += 1;
            }
            int reduceId = reduceActions.get(reduce);
            for (int symid = 0; symid < rule.rhs.size(); ++symid) {
              int id = rule.rhs.size() - symid;
              String symType = typeOf(rule.rhs.get(id - 1), rules);
              out.format("              %s sym%d = (%s) stack.pop();%n",
                  symType, id, symType);
            }
            out.format("              Symbol _result = reduce%d(", reduceId);
            for (int id = 1; id <= rule.rhs.size(); ++id) {
              if (id > 1) {
                out.print(", ");
              }
              out.format("sym%d", id);
            }
            out.println(");");
            out.println("              stack.push(_result);");
            for (int symid = 1; symid < rule.rhs.size(); ++symid) {
              out.println("              stateStack.pop();");
            }
            if (rule.rhs.isEmpty()) {
              out.println("              stateStack.push(state);");
            }
            out.format("              state = gotos[stateStack.peek()][%d];%n",
                sym2id.get(rule.lhs));
          } else if (action instanceof Shift) {
            Shift shift = (Shift) action;
            out.println("              stateStack.push(state);");
            out.println("              stack.push(next);");
            out.format("              state = %d;%n", shift.next.id());
            out.println("              next = nextToken(in);");
          }
          out.println("              break;");
        } else {
          out.println("              syntaxError(next);");
        }
        out.println("            }");
      }
      out.println("          }");
      out.println("          break;");
    }

    out.println("    }");
    out.println("    }");
    out.println("    return stack.pop();");
    out.println("  }");
    out.println();

    for (Map.Entry<Reduce, Integer> entry : reduceActions.entrySet()) {
      Reduce reduce = entry.getKey();
      int reduceId = entry.getValue();
      List<Symbol> rhs = reduce.rule.rhs;
      out.println();
      String lhsType = typeOf(reduce.rule.lhs, rules);
      out.format("  %s reduce%d(", lhsType, reduceId);
      for (int id = 1; id <= rhs.size(); ++id) {
        if (id > 1) {
          out.print(", ");
        }
        String symType = typeOf(rhs.get(id - 1), rules);
        int i = id - 1;
        if (i < reduce.rule.names.size()) {
          out.format("%s %s", symType, reduce.rule.names.get(i));
        } else {
          if (rhs.get(i).isTerminal()) {
            out.format("%s sym%d", symType, id);
          } else {
            out.format("%s %s", symType, rhs.get(i).name());
          }
        }
      }
      out.println(") {");
      out.format("    // %s%n", reduce.rule); // TODO: print short summary
      out.format("    if (DEBUG) System.out.println(\"%s\");%n", reduce.rule); // TODO: short summary
      if (!reduce.rule.action.isEmpty()) {
        out.format("    %s%n", reduce.rule.action);
      } else {
        if (rhs.isEmpty()) {
          out.format("    return new Nonterminal(\"%s\");%n", reduce.rule.lhs.name());
        } else {
          if (!reduce.rule.names.isEmpty()) {
            out.format("    return %s;%n", reduce.rule.names.get(reduce.rule.names.size()-1));
          } else {
            Symbol last = rhs.get(rhs.size() - 1);
            if (last.isTerminal()) {
              out.format("    return sym%d;%n", rhs.size() - 1);
            } else {
              out.format("    return %s;%n", last.name());
            }
          }
        }
      }
      out.println("  }");
    }

    // Print goto table.
    Map<Integer, Map<Symbol, Integer>> gotoMap = new HashMap<>();
    for (ItemSet set : itemSets) {
      Map<Symbol, ItemSet> trans = transitions.map.get(set);
      Map<Symbol, Action> setActions = transitions.actions.get(set);
      Map<Symbol, Integer> map = new HashMap<>();
      for (Symbol sym : nonterminals) {
        ItemSet next = trans.get(sym);
        if (next != null) {
          map.put(sym, next.id());
        } else {
          Action action = setActions.get(sym);
          if (action == Action.ACCEPT) {
            map.put(sym, -1);
          } else {
            // Error state.
            map.put(sym, 0);
          }
        }
      }
      gotoMap.put(set.id(), map);
    }
    out.println();
    out.println("  private static final int[][] gotos = {");
    out.print("    {");
    for (Symbol sym : nonterminals) {
      out.print(" 0,");
    }
    out.println(" },");
    for (int setId = 1; setId <= itemSets.size(); ++setId) {
      out.print("    {");
      for (Symbol sym : nonterminals) {
        out.print(" " + gotoMap.get(setId).get(sym) + ",");
      }
      out.println(" },");
    }
    out.println("  };");
    out.println();

    //println("/* Parser table:")
    //parser.printTable
    //out.println("*/");

    out.println("}");
  }

  public void printBeaverParser(PrintStream out, MyParser parser) throws IOException {
    if (!packageName.isEmpty()) {
      out.format("package %s;%n", packageName);
    }
    if (!header.isEmpty()) {
      // Emit embedded code.
      out.format("  %s%n", header);
    }
    out.println("import beaver.*;");
    out.println("import java.util.ArrayList;");
    out.println();
    out.println("import java.io.ByteArrayOutputStream;");
    out.println("import java.io.DataOutputStream;");
    out.println("import java.io.IOException;");
    out.println("import java.util.Stack;");
    out.println("import java.util.zip.DeflaterOutputStream;");
    out.println();
    out.println("// This is a parser generated by NeoBeaver.");
    out.format("public class %s extends beaver.Parser {%n", className);

    if (!embed.isEmpty()) {
      // Emit embedded code.
      out.format("  %s%n", embed);
    }

    TransitionTable transitions = parser.transitions;
    List<ItemSet> itemSets = parser.itemSets;

    // Sort symbols by number of occurrences in actions.
    Map<ItemSet, Set<Symbol>> usedSyms = new HashMap<>();
    for (ItemSet state : itemSets) {
      Set<Symbol> syms = new HashSet<>();
      syms.addAll(transitions.actions.get(state).keySet());
      syms.addAll(transitions.map.get(state).keySet());
      usedSyms.put(state, syms);
    }
    Map<Symbol, Integer> occurrences = new HashMap<>();
    for (Set<Symbol> symbols : usedSyms.values()) {
      for (Symbol sym : symbols) {
        if (occurrences.containsKey(sym)) {
          occurrences.put(sym, occurrences.get(sym) + 1);
        } else {
          occurrences.put(sym, 0);
        }
      }
    }
    occurrences.put(Symbol.EOF, Integer.MAX_VALUE); // EOF should be sorted first.

    // Here we ensure that terminal constants are generated for all declared terminals
    // by adding unused terminal with the lowest occurrence count.
    for (Symbol sym : syms) {
      if (sym.isTerminal() && !occurrences.containsKey(sym)) {
        occurrences.put(sym, Integer.MIN_VALUE); // EOF should be sorted first.
      }
    }

    List<Map.Entry<Symbol, Integer>> sorted = new ArrayList<>(occurrences.entrySet());
    Collections.sort(sorted, new Comparator<java.util.Map.Entry<Symbol, Integer>>() {
      @Override public int compare(Map.Entry<Symbol, Integer> o1, Map.Entry<Symbol, Integer> o2) {
        return o2.getValue().compareTo(o1.getValue());
      }
    });
    List<Symbol> nonterminals = new ArrayList<>();
    List<Symbol> terminals = new ArrayList<>();
    for (Map.Entry<Symbol, Integer> entry : sorted) {
      Symbol sym = entry.getKey();
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        nonterminals.add(sym);
      }
    }

    // Map symbols to IDs.
    // Nonterminal IDs are offset by last terminal ID so that terminals/ntas don't share ID.
    Map<Symbol, Integer> sym2id = new HashMap<>();
    Map<Integer, Symbol> id2sym = new HashMap<>();
    int nextSymId = 0;
    for (Symbol sym : terminals) {
      sym2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }
    for (Symbol sym : nonterminals) {
      sym2id.put(sym, nextSymId);
      id2sym.put(nextSymId, sym);
      nextSymId += 1;
    }

    // ID ranges of symbols used.
    Map<ItemSet, Set<Integer>> actionSyms = new HashMap<>();

    for (ItemSet state : itemSets) {
      Set<Integer> syms = new HashSet<>();
      for (Symbol sym : transitions.actions.get(state).keySet()) {
        if (sym.isTerminal()) {
          syms.add(sym2id.get(sym));
        }
      }
      actionSyms.put(state, syms);
    }

    Map<ItemSet, Set<Integer>> gotoSyms = new HashMap<>();
    for (ItemSet state : itemSets) {
      Set<Integer> syms = new HashSet<>();
      for (Symbol sym : transitions.actions.get(state).keySet()) {
        if (!sym.isTerminal()) {
          syms.add(sym2id.get(sym));
        }
      }
      for (Symbol sym : transitions.map.get(state).keySet()) {
        if (!sym.isTerminal()) {
          syms.add(sym2id.get(sym));
        }
      }
      gotoSyms.put(state, syms);
    }
    Map<ItemSet, Tuple<Integer, Integer>> actionRanges = new HashMap<>();
    for (ItemSet state : itemSets) {
      Set<Integer> syms = actionSyms.get(state);
      if (!syms.isEmpty()) {
        actionRanges.put(state, range(syms));
      }
    }
    Map<ItemSet, Tuple<Integer, Integer>> gotoRanges = new HashMap<>();
    for (ItemSet state : itemSets) {
      Set<Integer> syms = gotoSyms.get(state);
      if (!syms.isEmpty()) {
        gotoRanges.put(state, range(syms));
      }
    }

    //println("action ranges:")
    //println(actionRanges.map{case(k,v)=>k.id -> v})
    //println("got ranges:")
    //println(gotoSyms.map{case(k,v)=>k.id -> v})

    // Print token table.
    out.println("  public static class Terminals {");
    for (Symbol sym : terminals) {
      int tokId = sym2id.get(sym);
      if (sym.isNamed()) {
        out.format("    public static final short %s = %d;%n", sym.name(), tokId);
      } else if (sym == Symbol.EOF) {
        out.format("    public static final short EOF = %d;%n", tokId);
      } else if (sym.isTerminal()) {
        out.format("    public static final short TOKEN_%d = %d;%n", tokId, tokId);
      } else {
        throw new Error("Can't generate parser with unnamed nonterminal!");
      }
    }
    out.println();
    out.println("    public static final String[] NAMES = {");
    for (Symbol sym : terminals) {
      int tokId = sym2id.get(sym);
      if (sym.isNamed()) {
        out.format("        \"%s\",%n", sym.name());
      } else if (sym == Symbol.EOF) {
        out.format("        \"EOF\",%n");
      } else if (sym.isTerminal()) {
        out.format("        \"TOKEN_%d\",%n", tokId);
      } else {
        throw new Error("Can't generate parser with unnamed nonterminal!");
      }
    }
    out.println("    };");
    out.println("  }");
    out.println();

    out.println("  private final Action[] actions = {");

    Map<Reduce, Integer> actionMap = new HashMap<>();
    final Map<Reduce, Integer> uniqueActions = new HashMap<>();
    int actionId = 0;
    Map<Tuple<Symbol, Integer>, Integer> returnAction = new HashMap<>();

    Set<Integer> extraReturns = new HashSet<>();

    for (ItemSet set : itemSets) {
      Map<Symbol, Action> setActions = transitions.actions.get(set);
      for (Symbol tok : terminals) {
        Action act = setActions.get(tok);
        if (act instanceof Reduce) {
          Reduce reduce = (Reduce) act;
          if (!actionMap.containsKey(reduce)) {
            int ruleId = actionId;
            if (reduce.rule.action.isEmpty()) {
              Tuple<Symbol, Integer> key = Tuple.of(reduce.rule.lhs, reduce.rule.rhs.size());
              Integer matchedId = returnAction.get(key);
              if (matchedId != null) {
                // Reuse already generated return action.
                actionMap.put(reduce, matchedId);
              } else {
                returnAction.put(key, actionId);
                uniqueActions.put(reduce, actionId);
                actionMap.put(reduce, actionId);
                actionId += 1;
                int num = reduce.rule.rhs.size();
                switch (num) {
                  case 0:
                    out.format("    Action.NONE, // [%d] %s (default action: return null)%n",
                        ruleId, reduce.rule.shortDesc());
                    break;
                  case 1:
                    out.format("    Action.RETURN, // [%d] %s (default action: return symbol 1)%n",
                        ruleId, reduce.rule.shortDesc());
                    break;
                  default:
                    out.format("    RETURN%d, // [%d] %s (default action: return symbol %d)%n",
                        num, ruleId, reduce.rule.shortDesc(), num);
                    extraReturns.add(num);
                    break;
                }
              }
            } else {
              uniqueActions.put(reduce, actionId);
              actionMap.put(reduce, actionId);
              actionId += 1;
              out.format("    new Action() { // [%d] %s%n",
                  ruleId, reduce.rule.shortDesc());
              out.println("      public Symbol reduce(Symbol[] _symbols, int offset) {");
              List<Symbol> rhs = reduce.rule.rhs;
              for (int id = 0; id < rhs.size(); id += 1) {
                String symType = typeOf(rhs.get(id), rules);
                out.format("        final %s ", symType);
                if (reduce.rule.names.isEmpty()) {
                  Symbol sym = rhs.get(id);
                  if (!sym.isTerminal()) {
                    out.print(sym.name());
                  } else if (sym.isNamed()) {
                    out.print(sym.name());
                  } else {
                    out.format("sym%d", id + 1);
                  }
                } else{
                  out.print(reduce.rule.names.get(id));
                }
                if (symType.equals("Symbol")) {
                  // No cast needed.
                  out.format(" = _symbols[offset + %d];%n", id + 1);
                } else {
                  out.format(" = (%s) _symbols[offset + %d].value;%n", symType, id + 1);
                }
              }
              out.format("        %s%n", reduce.rule.action);
              out.println("      }");
              out.println("    },");
            }
          }
        }
      }
    }

    out.println("  };");

    for (Integer num : extraReturns) {
      out.println();
      out.format("      static final Action RETURN%d = new Action() {%n", num);
      out.println("        public Symbol reduce(Symbol[] _symbols, int offset) {");
      out.format("          return _symbols[offset + %d];%n", num);
      out.println("        }");
      out.format("      };");
    }

    int numterm = terminals.size();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    DeflaterOutputStream deflater = new DeflaterOutputStream(bos);
    DataOutputStream dout = new DataOutputStream(deflater);

    Map<Integer, Integer> actionTable = new HashMap<>();
    Map<Integer, Integer> lookaheads = new HashMap<>();
    Map<Integer, Integer> defaultActions = new HashMap<>();

    // Can not reuse the same offset for different actions because then the
    // lookaheads will overlap.  The usedOffsets set tracks already-used
    // offsets and prevents this error.
    // It is okay if action and goto rows use the same offset because they
    // don't use the same lookahead indices (terminals vs nonterminals).
    // It is okay if two different goto rows share the same lookaheads because
    // by construction they should never see the wrong lookahead tokens.
    Set<Integer> usedOffsets = new HashSet<>();

    List<ItemSet> sortedItems = new ArrayList<>(itemSets);
    Collections.sort(sortedItems, new Comparator<ItemSet>() {
      @Override public int compare(ItemSet o1, ItemSet o2) {
        return Integer.compare(o1.id(), o2.id());
      }
    });

    Map<Integer, Map<Integer, Integer>> acts = new HashMap<>();
    for (ItemSet set : itemSets) {
      Map<Integer, Integer> map = new HashMap<>();
      Map<Symbol, Action> setActions = transitions.actions.get(set);
      Tuple<Integer, Integer> range = actionRanges.get(set);
      if (range != null) {
        int min = range.first;
        int max = range.second;
        for (int id = min; id <= max; ++id) {
          Action action = setActions.get(id2sym.get(id));
          if (action instanceof Reduce) {
            Reduce reduce = (Reduce) action;
            map.put(id, -actionMap.get(reduce) - 1);
          } else if (action instanceof Shift) {
            Shift shift = (Shift) action;
            map.put(id, shift.next.id());
          }
        }
      }
      acts.put(set.id(), map);
    }

    // Beaver expects state IDs to start on 1. Internally we also use 1-indexed states.
    Map<Integer, Map<Integer, Integer>> gotos = new HashMap<>();
    int errorAct = 0;
    int acceptAct = -uniqueActions.size() - 1;
    for (ItemSet set : itemSets) {
      Map<Symbol, ItemSet> trans = transitions.map.get(set);
      Map<Symbol, Action> setActions = transitions.actions.get(set);
      Map<Integer, Integer> map = new HashMap<>();
      for (Symbol sym : nonterminals) {
        ItemSet next = trans.get(sym);
        if (next != null) {
          map.put(sym2id.get(sym), next.id());
        } else {
          Action action = setActions.get(sym);
          if (action == Action.ACCEPT) {
            map.put(sym2id.get(sym), acceptAct);
          }
        }
      }
      gotos.put(set.id(), map);
    }

    // Generate action table.
    Map<Integer, Integer> actionOffsets = new HashMap<>();
    int actOffset = 0;
    for (ItemSet set : sortedItems) {
      Tuple<Integer, Integer> range = actionRanges.get(set);
      if (range != null) {
        int min = range.first;
        int max = range.second;
        //System.out.format("set %d range: (%d, %d)%n", set.id(), min, max);
        Collection<Integer> actions = acts.get(set.id()).values();
        if (allSame(actions)) {
          Integer firstAction = actions.iterator().next();
          defaultActions.put(set.id(), firstAction);
          actionOffsets.put(set.id(), Integer.MIN_VALUE);
        } else {
          boolean placed = false;
          int offset = actOffset;
          actOffset += terminals.size(); // TODO: totally flaky and won't work for real, needs to be fixed!!
          while (!placed) {
            int setId = 1;
            while (!placed && setId < set.id()) {
              if (acts.get(setId) == acts.get(set.id())) { // TODO: proper equality comparison.
                placed = true;
                actionOffsets.put(set.id(), actionOffsets.get(setId));
              }
              setId += 1;
            }
            if (!actionTable.containsKey(offset)) {
              placed = tryPlaceActions(actionTable, lookaheads, acts, set, offset, min, max);
              if (placed) {
                actionOffsets.put(set.id(), offset - min);
              }
            }
            if (!placed) {
              offset += 1;
            }
          }
        }
      } else {
        //System.out.format("set %d range: null%n", set.id());
        //System.out.println(set);
        actionOffsets.put(set.id(), Integer.MIN_VALUE);
      }
    }

    // Generate goto table.
    Map<Integer, Integer> gotoOffsets = new HashMap<>();
    for (ItemSet set : sortedItems) {
      Tuple<Integer, Integer> range = gotoRanges.get(set);
      if (range != null) {
        int min = range.first;
        int max = range.second;
        Collection<Integer> actions = gotos.get(set.id()).values();
        if (!defaultActions.containsKey(set.id()) && allSame(actions)) {
          if (actions.isEmpty()) {
            System.err.format("err set: %s (%d, %d)", set, min, max);
            System.err.println("gotos: " + gotoSyms.get(set));
            System.err.println("actions:");
            System.err.println( transitions.actions.get(set));
            for (Action act : transitions.actions.get(set).values()) {
              System.out.println(act);
            }
            for (Symbol sym : transitions.actions.get(set).keySet()) {
              if (!sym.isTerminal()) {
                System.err.println("  " + sym2id.get(sym));
                System.err.println("  -> " + transitions.actions.get(set).get(sym));
              }
            }
            System.err.println("transitions:");
            for (ItemSet next : transitions.map.get(set).values()) {
              System.out.println(next);
            }
            for (Symbol sym : transitions.map.get(set).keySet()) {
              if (!sym.isTerminal()) {
                System.err.println("  " + sym2id.get(sym));
              }
            }
          }
          Integer firstAction = actions.iterator().next();
          defaultActions.put(set.id(), firstAction);
          gotoOffsets.put(set.id(), Integer.MIN_VALUE);
        } else {
          boolean placed = false;
          int offset = 0;
          while (!placed) {
            int setId = 1;
            while (!placed && setId < set.id()) {
              if (gotos.get(setId) == gotos.get(set.id())) {
                placed = true;
                gotoOffsets.put(set.id(), gotoOffsets.get(setId));
              }
              setId += 1;
            } if (!actionTable.containsKey(offset)) {
              placed = tryPlaceGotos(actionTable, lookaheads, usedOffsets, gotos,
                  set, offset, min, max);
              if (placed) {
                gotoOffsets.put(set.id(), (offset - min));
              }
            } if (!placed) {
              offset += 1;
            }
          }
        }
      } else {
        gotoOffsets.put(set.id(), Integer.MAX_VALUE);
      }
    }

    // Write action lookahead table.
    int numActions = range(actionTable.keySet()).second + 1;
    dout.writeInt(numActions);
    for (int index = 0; index < numActions; index += 1) {
      Integer act = actionTable.get(index);
      if (act != null) {
        dout.writeShort(act);
      } else {
        dout.writeShort(0);
      }
    }

    // Write goto lookahead table.
    for (int index = 0; index < numActions; index += 1) {
      Integer sym = lookaheads.get(index);
      if (sym != null) {
        dout.writeShort(sym);
      } else {
        dout.writeShort(-1);
      }
    }

    // Write action offset table.
    dout.writeInt(itemSets.size() + 1);
    dout.writeInt(Integer.MIN_VALUE); // Unused offset.
    for (int id = 1; id <= itemSets.size(); id += 1) {
      dout.writeInt(actionOffsets.get(id));
    }

    // Write goto offset table.
    dout.writeInt(Integer.MIN_VALUE); // Unused offset.
    for (int id = 1; id <= itemSets.size(); id += 1) {
      dout.writeInt(gotoOffsets.get(id));
    }

    // Write default actions table.
    dout.writeInt(itemSets.size() + 1);
    dout.writeShort(0); // Unused offset.
    for (int id = 1; id <= itemSets.size(); id += 1) {
      Integer act = defaultActions.get(id);
      if (act != null) {
        dout.writeShort(act);
      } else {
        dout.writeShort(0);
      }
    }

    // Write rule info.
    dout.writeInt(uniqueActions.keySet().size()); // Not compressed.
    List<Reduce> sortedActions = new ArrayList<>(uniqueActions.keySet());
    Collections.sort(sortedActions, new Comparator<Reduce>() {
      @Override public int compare(Reduce o1, Reduce o2) {
        return Integer.compare(uniqueActions.get(o1), uniqueActions.get(o2));
      }
    });
    for (Reduce reduce : sortedActions) {
      int act = -uniqueActions.get(reduce) - 1;
      int info = (sym2id.get(reduce.rule.lhs) << 16) | reduce.rule.rhs.size();
      dout.writeInt(info);
    }

    dout.writeShort(terminals.size() + nonterminals.size()); // Error terminal.
    dout.close();
    deflater.close();

    out.println();
    out.println("  static final ParsingTables PARSING_TABLES = new ParsingTables(");
    String tables = Util.base64Encode(bos.toByteArray());
    for (int offset = 0; offset < tables.length(); offset += 71) {
      if (offset > 0) {
        out.println(" +");
      }
      String part = tables.substring(offset);
      part = part.substring(0, Math.min(part.length(), 71));
      out.format("    \"%s\"", part);
    }
    out.println(");");
    out.println();

    out.format("  public %s() {%n", className);
    out.println("    super(PARSING_TABLES);");
    out.println("  }");
    out.println();

    out.println("  protected Symbol invokeReduceAction(int rule_num, int offset) {");
    out.println("    return actions[rule_num].reduce(_symbols, offset);");
    out.println("  }");
    out.println("}");
  }

  boolean tryPlaceActions(
      Map<Integer, Integer> actionTable,
      Map<Integer, Integer> lookaheads,
      Map<Integer, Map<Integer, Integer>> acts,
      ItemSet set, int offset, int min, int max) {
    Map<Integer, Integer> map = acts.get(set.id());
    for (int id = min + 1; id <= max; id += 1) {
      int index = offset + id - min;
      if ((map.containsKey(id)) && actionTable.containsKey(index)) {
        return false;
      }
    }
    for (int id = min; id <= max; id += 1) {
      int index = offset + id - min;
      Integer act = map.get(id);
      if (act != null) {
        actionTable.put(index, act);
        lookaheads.put(index, id);
      }
    }
    return true;
  }

  boolean tryPlaceGotos(
      Map<Integer, Integer> actionTable,
      Map<Integer, Integer> lookaheads,
      Set<Integer> usedOffsets,
      Map<Integer, Map<Integer, Integer>> gotos,
      ItemSet set, int offset, int min, int max) {
    if (usedOffsets.contains(offset - min)) {
      return false;
    }
    Map<Integer, Integer> map = gotos.get(set.id());
    for (int id = min + 1; id <= max; id += 1) {
      int index = offset + id - min;
      if ((map.containsKey(id)) && actionTable.containsKey(index)) {
        return false;
      }
    }
    for (int id = min; id <= max; id += 1) {
      int index = offset + id - min;
      Integer act = map.get(id);
      if (act != null) {
        actionTable.put(index, act);
        lookaheads.put(index, id);
      }
    }
    usedOffsets.add(offset - min);
    return true;
  }

  public void checkProblems(ProblemHandler problems, Set<Symbol> roots) {
    Set<Symbol> terminals = new HashSet<>();
    for (Symbol sym : syms) {
      if (sym.isTerminal()) {
        terminals.add(sym);
      } else {
        roots.add(sym);
      }
    }
    for (Rule rule : rules) {
      for (Symbol sym : rule.rhs) {
        if (sym.isTerminal()) {
          terminals.remove(sym);
        } else {
          roots.remove(sym);
        }
      }
    }
    roots.remove(Symbol.GOAL);
    if (!roots.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      sb.append("found unused nonterminal(s):");
      for (Symbol root : roots) {
        sb.append(String.format("%n  %s was declared at %s", root, root.pos()));
      }
      problems.warn(sb.toString());
    }
    terminals.remove(Symbol.EOF);
    if (!terminals.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      sb.append("found unused terminal(s):");
      for (Symbol term : terminals) {
        sb.append(String.format("%n  %s was declared at %s", term, term.pos()));
      }
      problems.warn(sb.toString());
    }
  }

  public Symbol sym(int index) {
    return symIndex.get(index);
  }

  /**
   * Returns the name of the terminal used to determine precedence for this action.
   *
   * <p>The rightmost terminal is used to determine rule precedence (according to Beaver specification).
   */
  public String precedenceTerminal(Action action) {
    if (action instanceof Shift) {
      return ((Shift) action).sym.name();
    } else if (action instanceof Reduce) {
      Rule rule = ((Reduce) action).rule;
      if (!rule.precedence.isEmpty()) {
        return rule.precedence;
      }
      for (int i = rule.rhs.size() - 1; i >= 0; --i) {
        Symbol sym = rule.rhs.get(i);
        if (sym.isTerminal() && precedence.containsKey(sym.name())) {
          return sym.name();
        }
      }
    }
    return "?term";
  }
}
