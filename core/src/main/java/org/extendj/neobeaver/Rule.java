/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Rule {
  public final int id;
  public final Symbol lhs;
  public final List<Symbol> rhs;
  public final String action; // Semantic action - nullable.
  public final List<String> names;  // RHS component names - nullable.
  public final String type;
  public final String precedence; // Precedence symbol for this rule.

  public Rule(Symbol lhs, List<Symbol> rhs) {
    this(-1, lhs, rhs, "", defaultNames(rhs), "Symbol", "");
  }

  public Rule(int id, Symbol lhs, List<Symbol> rhs, String action,
      List<String> names, String type) {
    this(id, lhs, rhs, action, names, type, "");
  }

  public Rule(int id, Symbol lhs, List<Symbol> rhs, String action,
      List<String> names, String type, String precedence) {
    this.id = id;
    this.lhs = lhs;
    this.rhs = rhs;
    this.action = action.trim();
    this.names = names;
    this.type = type;
    this.precedence = precedence;
  }

  private static List<String> defaultNames(List<Symbol> rhs) {
    List<String> names = new ArrayList<>();
    for (Symbol sym : rhs) {
      String name = sym.actionName();
      int id = 1;
      while (names.contains(name)) {
        name = name + id;
        id += 1;
      }
      names.add(sym.actionName());
    }
    return names;
  }

  /**
   * Rules for the canonical form of this rule.
   */
  public Collection<? extends Rule> canonical() {
    Collection<Rule> result = new ArrayList<>();
    for (List<Symbol> rhs : split(this.rhs)) {
      result.add(new Rule(-1, lhs, rhs, action, names, type, precedence));
    }
    return result;
  }

  static Collection<List<Symbol>> split(List<Symbol> rhs) {
    if (rhs.isEmpty()) {
      return Collections.singletonList(Collections.<Symbol>emptyList());
    }
    Symbol head = rhs.get(0);
    List<Symbol> tail = new ArrayList<>(rhs);
    tail.remove(0);
    Collection<List<Symbol>> result = new ArrayList<>();
    if (head instanceof OptionalComponent) {
      OptionalComponent opt = (OptionalComponent) head;
      for (List<Symbol> suffix : split(tail)) {
        List<Symbol> add = new ArrayList<>(suffix.size() + 1);
        add.add(opt.sym);
        add.addAll(suffix);
        result.add(add);
      }
      return result;
    } else if (head instanceof ListComponent) {
      ListComponent list = (ListComponent) head;
      for (List<Symbol> suffix : split(tail)) {
        List<Symbol> add = new ArrayList<>(suffix.size() + 1);
        add.add(list.sym);
        add.addAll(suffix);
        result.add(add);
      }
      return result;
    } else {
      for (List<Symbol> suffix : split(tail)) {
        List<Symbol> add = new ArrayList<>(suffix.size() + 1);
        add.add(head);
        add.addAll(suffix);
        result.add(add);
      }
      return result;
    }
  }

  public String type() {
    return type;
  }

  /**
   * Test if the right-hand-side of this rule is nullable,
   * according to a nullable bitset.
   */
  public boolean rhsNullable(BitSet nullable) {
    for (Symbol sym : rhs) {
      if (!nullable.get(sym.id())) {
        return false;
      }
    }
    return true;
  }

  @Override public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(lhs);
    sb.append(" = ");
    boolean first = true;
    for (Symbol sym : rhs) {
      if (!first) {
        sb.append(" ");
      }
      first = false;
      sb.append(sym);
    }
    return sb.toString();
  }

  public String shortDesc() {
    StringBuilder buf = new StringBuilder();
    buf.append(lhs);
    buf.append(" = ");
    for (Symbol tok : rhs) {
      if (buf.length() != 0) {
        buf.append(" ");
      }
      buf.append(tok);
    }
    return buf.toString();
  }

  public Collection<? extends Rule> extraRules() {
    List<Rule> extra = new ArrayList<>();
    for (Symbol sym : rhs) {
      extra.addAll(sym.extraRules());
    }
    return extra;
  }

  public String name() {
    if (id == -1) {
      return "r?";
    } else {
      return "r" + id;
    }
  }

  @Override public int hashCode() {
    return id ^ lhs.hashCode() ^ rhs.hashCode();
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof Rule) {
      Rule other = (Rule) obj;
      return id == other.id && lhs == other.lhs && rhs.equals(other.rhs);
    }
    return false;
  }
}
