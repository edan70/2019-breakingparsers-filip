/*
  Modified by Filip Johansson, 2019
*/

/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import org.extendj.neobeaver.ast.GGrammar;
import static org.extendj.neobeaver.GenerateTests.generateTests;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class NeoBeaver {
  public static void main(String[] args) throws IOException {
    Set<String> files = new HashSet<>();
    Set<String> options = new HashSet<>();
    for (String arg : args) {
      if (arg.startsWith("-")) {
        options.add(arg);
      } else {
        files.add(arg);
      }
    }

    if (args.length < 1) {
      System.err.println("No input files specified!");
      System.exit(1);
      return;
    }

    boolean errored = run(files, Paths.get(""), options);
    if (errored) {
      System.exit(1);
    }
  }

  /**
   * @param files
   * @param destDir base directory for generated files
   * @param options
   * @throws IOException
   * @return {@code true} if there were any errors
   */
  public static boolean run(Collection<String> files, Path destDir, Set<String> options) throws IOException {
    ProblemLogger problems = new ProblemLogger();
    TraceHandler trace;
    if (options.contains("--trace")) {
      trace = new TraceHandlerImpl("NeoBeaver");
    } else {
      trace = nullTrace();
    }
    try {
      if (options.contains("--tokenize") || options.contains("-t")) {
        for (String filename : files) {
          try (Scanner scanner = new Scanner(new FileReader(new File(filename)))) {
            while (true) {
              Parser.Token sym = scanner.nextToken();
              System.out.format("%4d: %s%n", sym.getId(), sym);
              if (sym.getId() == Parser.Tokens.EOF) {
                break;
              }
            }
          }
        }
      } else if (options.contains("--generatetests")) {
        for (String fileName : files) {
          try {
            System.out.println("Started generating tests.");
            generateTests(fileName, destDir);
            System.out.println("Finished generating tests.");
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      } else {
        for (String filename : files) {
          process(trace, problems, Paths.get(filename), destDir, options);
          problems.report();
        }
      }
    } finally {
      trace.sendTo("localhost", 6834);
    }
    problems.report();
    return problems.errored();
  }

  public static TraceHandler nullTrace() {
    final TraceEvent nullClose = new TraceEvent() {
      @Override
      public void close() {
      }
    };
    return new TraceHandler() {
      @Override
      public TraceEvent event(String name) {
        return nullClose;
      }

      @Override
      public TraceEvent event(String name, String metadata) {
        return nullClose;
      }

      @Override
      public void sendTo(String host, int port) {
      }
    };
  }

  public static void process(TraceHandler trace,
      ProblemHandler problems,
      Path path,
      Path destDir,
      Set<String> options) throws IOException {
    if (!Files.exists(path)) {
      problems.errorf("parser specification file does not exist: %s", path);
      return;
    }
    try (Scanner scanner = new Scanner(new FileReader(path.toFile()))) {
      GGrammar grammarSpec;
      ProblemScope scope = new ProblemScope(problems);
      try (TraceEvent ignored = trace.event("parse grammar")) {
        Parser grammarParser = new Parser();
        grammarSpec = (GGrammar) grammarParser.parse(scanner);
      } catch (Parser.SyntaxError e) {
        scope.error(e.getMessage());
        return;
      }
      Grammar grammar;
      try (TraceEvent ignored = trace.event("build parser")) {
        BeaverParserBuilder builder = new BeaverParserBuilder(scope);
        String defaultParserName = fileNameWithoutExtension(path);
        if (!defaultParserName.isEmpty()) {
          builder.defaultParserName(defaultParserName);
        }
        builder.grammarSpec(grammarSpec, trace);
        try (TraceEvent ignored1 = trace.event("Grammar.build")) {
          grammar = builder.build(trace);
        }
      }
      Set<Symbol> unused = new HashSet<>();
      try (TraceEvent ignored = trace.event("check problems")) {
        grammar.checkProblems(scope, unused);
      }
      MyParser parser;
      try (TraceEvent ignored = trace.event("build parser")) {
        parser = grammar.buildParser(scope, trace);
      }
      try (TraceEvent ignored = trace.event("check problems")) {
        parser.checkProblems(grammar, scope, unused, options.contains("--unreachable-error"));
        parser.transitions.printConflicts(scope, grammar);
      }
      if (scope.errored()) {
        return;
      }
      if (options.contains("--print-table") || options.contains("-table")) {
        parser.transitions.printTables(parser.grammar, parser.itemSets);
        parser.printTables();
      }
      if (options.contains("--print-table") || options.contains("-p")) {
        parser.printTables();
      }
      if (options.contains("-i")) {
        parser.printItems();
      }
      if (options.contains("-g")) {
        parser.printGraph();
      }
      if (options.contains("--standalone") || options.contains("-s")) {
        // Generate standalone parser.
        parser.printParser();
      }
      if (options.contains("--classic")) {
        // Print classic Beaver specification.
        parser.printBeaverSpec();
      }
      if (options.contains("--test")) {
        // Print classic Beaver specification.
        parser.printBeaverTestSpec();
      }
      if (!Files.exists(destDir)) {
        scope.errorf("Error: output directory %s does not exist.", destDir);
        return;
      }
      if (!grammar.packageName.isEmpty()) {
        destDir = destDir.resolve(grammar.packageName.replace(".", File.separator));
        if (!Files.exists(destDir)) {
          System.out.println("Creating output directory: " + destDir);
          Files.createDirectories(destDir);
        }
      }
      Path dest = destDir.resolve(String.format("%s.java", grammar.className));
      if (options.contains("--beaver")) {
        try (FileOutputStream fout = new FileOutputStream(dest.toFile());
             PrintStream out = new PrintStream(fout)) {
          grammar.printBeaverParser(out, parser);
        }
      }
    }
  }

  private static String fileNameWithoutExtension(Path path) {
    String name = path.getFileName().toString();
    int index = name.lastIndexOf('.');
    if (index > 0) {
      name = name.substring(0, index);
    }
    return name;
  }

}
