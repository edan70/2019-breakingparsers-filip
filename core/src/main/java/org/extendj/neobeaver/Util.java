/*
  Modified by Filip Johansson, 2019
*/

/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.Files;
import java.nio.file.FileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;

import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.Parser;
import org.extendj.neobeaver.Scanner;

public class Util {
  public static String escape(String str) {
    return str.replace("\\", "\\\\").replace("\"", "\\\"");
  }

  public static String base64Encode(byte[] bytes) {
    ByteArrayOutputStream b64 = new java.io.ByteArrayOutputStream();
    for (int i = 0; i < bytes.length; i += 3) {
      if (i + 2 < bytes.length) {
        int b0 = bytes[i + 0] & 0xFF;
        int b1 = bytes[i + 1] & 0xFF;
        int b2 = bytes[i + 2] & 0xFF;
        // 000000 001111 111122 222222
        encode(b64, (b0 >> 2) & 0x3F);
        encode(b64, ((b0 & 0x3) << 4) | ((b1 >> 4) & 0xF));
        encode(b64, ((b1 & 0xF) << 2) | ((b2 >> 6) & 0x3));
        encode(b64, b2 & 0x3F);
      } else if (i + 1 < bytes.length) {
        int b0 = bytes[i + 0] & 0xFF;
        int b1 = bytes[i + 1] & 0xFF;
        encode(b64, (b0 >> 2) & 0x3F);
        encode(b64, ((b0 & 0x3) << 4) | ((b1 >> 4) & 0xF));
        encode(b64, (b1 & 0xF) << 2);
        b64.write('=');
      } else {
        int b0 = bytes[i + 0] & 0xFF;
        encode(b64, (b0 >> 2) & 0x3F);
        encode(b64, (b0 & 0x3) << 4);
        b64.write('=');
        b64.write('=');
      }
    }
    return new String(b64.toByteArray());
  }

  public static byte[] base64Decode(String input) {
    int outputSize = (3 * input.length() + 3) / 4;
    byte[] output = new byte[outputSize];
    int outpos = 0;
    int step = 0;
    for (int i = 0; i < input.length(); ++i) {
      char ch = input.charAt(i);
      int value;
      if (ch >= '0' && ch <= '9') {
        value = ch - '0';
      } else if (ch >= 'A' && ch <= 'Z') {
        value = 10 + (ch - 'A');
      } else if (ch >= 'a' && ch <= 'z') {
        value = 36 + (ch - 'a');
      } else if (ch == '#') {
        value = 62;
      } else if (ch == '$') {
        value = 63;
      } else if (ch == '=') {
        value = 0;
      } else {
        throw new Error(String.format("Unexpected Base64 character: '%c' in input.", ch));
      }
      switch (step) {
        case 0:
          output[outpos] = (byte) (value << 2);
          step = 1;
          break;
        case 1:
          output[outpos++] |= (byte) (value >> 4);
          output[outpos] = (byte) (0xFF & (value << 4));
          step = 2;
          break;
        case 2:
          output[outpos++] |= (byte) (value >> 2);
          output[outpos] = (byte) (0xFF & (value << 6));
          step = 3;
          break;
        case 3:
          output[outpos++] |= (byte) value;
          step = 0;
          break;
      }
    }
    return output;
  }

  private static final char[] BASE64 = {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
      'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
      'U', 'V', 'W', 'X', 'Y', 'Z',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
      'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
      'u', 'v', 'w', 'x', 'y', 'z',
      '#', '$'
  };

  private static void encode(ByteArrayOutputStream bytes, int b) {
    bytes.write(BASE64[b]);
  }

  public static void deleteDirectory(Path path) throws IOException {
    Files.walkFileTree(path, new FileVisitor<Path>() {
      @Override public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
          throws IOException {
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
          throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult visitFileFailed(Path file, IOException exc)
          throws IOException {
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult postVisitDirectory(Path dir, IOException exc)
          throws IOException {
        if (exc != null) {
          throw exc;
        }
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  public static GGrammar parse(Path inputPath) throws Exception {
    Scanner scanner = new Scanner(Files.newBufferedReader(inputPath));
    Parser parser = new Parser();
    GGrammar grammar = (GGrammar) parser.parse(scanner);
    return grammar;
  }

}
