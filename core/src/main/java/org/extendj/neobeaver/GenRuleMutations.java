/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

package org.extendj.neobeaver;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import org.extendj.neobeaver.ast.ASTNode;
import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GComponent;

import org.extendj.neobeaver.RuleMutation;

public class GenRuleMutations {

	public static List<RuleMutation> generateRuleMutations(GGrammar grammar) {

		List<RuleMutation> mutations = new LinkedList<>();
		
		for (GRule rule : grammar.rules()) {

			int ruleLength = rule.getGComponents().getNumChild();

			for (int i = 0; i <= ruleLength; i++) {
				Set<String> left = rule.left(i);
				Set<String> precedeRight = grammar.precede(rule.right(i+1));

				if (i < ruleLength && !hasIntersection(left, precedeRight)) {
					mutations.add(new SymbolDeletion(rule, i));
				}

				for (String sym : grammar.symbols()) {
					if (!grammar.isStartSymbol(sym)) {
						precedeRight = grammar.precede(rule.right(sym, i));

						if (!hasIntersection(left, precedeRight)) {
							mutations.add(new SymbolInsertion(rule, i, sym));
						}

						if (i >= ruleLength) {
							continue;
						}

						precedeRight = grammar.precede(rule.right(sym, i+1));

						if (!hasIntersection(left, precedeRight)) {
							mutations.add(new SymbolSubstitution(rule, i, sym));
						}
					}
				}


			}

		}

		return mutations;
	}

	private static boolean hasIntersection(Set<String> syms1, Set<String> syms2) {
		Set<String> larger;
		Set<String> smaller;

		if (syms1.size() > syms2.size()) {
			larger = syms1;
			smaller = syms2;
		} else {
			larger = syms2;
			smaller = syms1;
		}

		for (String sym : smaller) {
			if (larger.contains(sym)) {
				return true;
			}
		}

		return false;
	}

}
