package org.extendj.neobeaver;

import java.io.IOException;

public interface TraceHandler {
  TraceEvent event(String name);
  TraceEvent event(String name, String metadata);
  void sendTo(String host, int port) throws IOException;
}
