package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.extendj.neobeaver.Grammar.VERBOSE;
import static org.extendj.neobeaver.Grammar.typeOf;

public class GrammarBuilder {
  public static Grammar build(TraceHandler trace, List<Rule> rules, Set<Symbol> extraTerminals,
                              Symbol goalSym, String embed, String header, String className, String packageName,
                              Set<String> left, Set<String> right, Set<String> nonassoc, Map<String, Integer> precedence) {
    Rule goalRule;
    try (TraceEvent ignored = trace.event("goal")) {
      goalRule = new Rule(-1,
          Symbol.GOAL, listOf(goalSym, Symbol.EOF),
          String.format("return %s;", goalSym.actionName()),
          Collections.<String>emptyList(),
          typeOf(goalSym, rules));
    }


    // Build canonical rules set.
    Collection<Rule> canonical;
    try (TraceEvent ignored = trace.event("canonical")) {
      canonical = new HashSet<>();
      canonical.addAll(goalRule.canonical()); // Don't forget to add the goal rule.
      canonical.addAll(Grammar.canonicalRules(rules));
    }

    Set<Symbol> syms;
    List<Rule> enumerated;
    Set<Symbol> nonterminals;
    Set<Symbol> terminals;
    try (TraceEvent ignored = trace.event("enumerate")) {
      // Enumerate all rules - assign an ID used in bitsets.
      enumerated = enumerate(canonical);
      syms = new HashSet<>(extraTerminals);
      syms.addAll(symbols(enumerated)); // Find symbols in productions.
      nonterminals = new HashSet<>(syms.size());
      terminals = new HashSet<>(syms.size());

      for (Symbol sym : syms) {
        if (sym.isTerminal()) {
          terminals.add(sym);
        } else {
          nonterminals.add(sym);
        }
      }
    }

    Map<Symbol, Collection<Rule>> byLhs;
    Map<Symbol, Set<Symbol>> first;
    Map<Symbol, Set<Symbol>> follow;
    BitSet nullable;
    Map<Symbol, Set<Item>> extension;
    try (TraceEvent ignored = trace.event("nullable")) {
      byLhs = mapByLhs(enumerated, syms);
      first = new HashMap<>();
      follow = new HashMap<>();
      nullable = new BitSet(syms.size());
      extension = new HashMap<>();

      // Compute nullable set first.

      BitSet diff = new BitSet(syms.size());
      do {
        diff.clear();
        for (Symbol sym : nonterminals) {
          if (!nullable.get(sym.id())) {
            for (Rule rule : byLhs.get(sym)) {
              if (rule.rhsNullable(nullable)) {
                diff.set(sym.id());
              }
            }
          }
        }
        nullable.or(diff);
      } while (!diff.isEmpty());
    }

    try (TraceEvent ignored = trace.event("first+follow")) {
      for (Symbol sym : syms) {
        Set<Symbol> followSet = new HashSet<>();
        follow.put(sym, followSet);
        Set<Symbol> firstSet = new HashSet<>();
        first.put(sym, firstSet);
        if (sym.isTerminal()) {
          firstSet.add(sym);
        }
      }
      // Compute FIRST & FOLLOW sets.
      boolean change;
      do {
        change = false;
        for (Symbol sym : nonterminals) {
          Set<Symbol> nextFirst = new HashSet<>();
          for (Rule rule : byLhs.get(sym)) {
            for (Symbol rhs : rule.rhs) {
              nextFirst.addAll(first.get(rhs));
              if (!nullable.get(rhs.id())) {
                break;
              }
            }
          }
          Set<Symbol> nextFollow = new HashSet<>();
          for (Rule rule : enumerated) {
            int index = rule.rhs.indexOf(sym);
            if (index >= 0) {
              boolean addLhs = true;
              for (int j = index + 1; j < rule.rhs.size(); ++j) {
                Symbol rhsSym = rule.rhs.get(j);
                nextFollow.addAll(first.get(rhsSym));
                if (rhsSym == sym || !nullable.get(rhsSym.id())) {
                  addLhs = false;
                  break;
                }
              }
              if (addLhs) {
                nextFollow.addAll(follow.get(rule.lhs));
              }
            }
          }
          Set<Symbol> firstSet = first.get(sym);
          if (!firstSet.containsAll(nextFirst)) {
            firstSet.addAll(nextFirst);
            change = true;
          }
          Set<Symbol> followSet = follow.get(sym);
          if (!followSet.containsAll(nextFollow)) {
            followSet.addAll(nextFollow);
            change = true;
          }
        }
      } while (change);

      if (VERBOSE) {
        // Print the FIRST, FOLLOW sets.
        for (Symbol sym : nonterminals) {
          System.out.println(sym);
          System.out.println("NULLABLE: " + nullable.get(sym.id()));
          System.out.println("  FIRST:");
          for (Symbol firstSym : first.get(sym)) {
            System.out.println("    " + firstSym);
          }
          System.out.println("  FOLLOW:");
          for (Symbol followSym : follow.get(sym)) {
            System.out.println("    " + followSym);
          }
        }
      }
    }

    return new Grammar(rules, enumerated, goalSym,
        goalRule, syms, nullable, first, follow, extension,
        new ExtensionBuilderLr1(byLhs, nullable, first, follow),
        embed, header, className, packageName,
        left, right, nonassoc, precedence);
  }

  private static <T> List<T> listOf(T... items) {
    List<T> list = new ArrayList<>();
    for (T item : items) {
      list.add(item);
    }
    return list;
  }

  private static Map<Symbol, Collection<Rule>> mapByLhs(Collection<Rule> rules,
                                                        Collection<Symbol> syms) {
    Map<Symbol, Collection<Rule>> result = new HashMap<>();
    for (Symbol sym : syms) {
      result.put(sym, new ArrayList<Rule>());
    }
    for (Rule rule : rules) {
      result.get(rule.lhs).add(rule);
    }
    return result;
  }

  private static Set<Symbol> symbols(Collection<Rule> rules) {
    Set<Symbol> symbols = new HashSet<>();
    for (Rule rule : rules) {
      symbols.add(rule.lhs);
      symbols.addAll(rule.rhs);
    }
    return symbols;
  }

  private static List<Rule> enumerate(Collection<Rule> rules) {
    int id = 0;
    List<Rule> result = new ArrayList<>();
    for (Rule rule : rules) {
      result.add(new Rule(id, rule.lhs, rule.rhs, rule.action, rule.names, rule.type, rule.precedence));
      id += 1;
    }
    return result;
  }

}
