/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static org.extendj.neobeaver.TestUtil.deleteDirectory;
import static org.extendj.neobeaver.TestUtil.normalize;
import static org.extendj.neobeaver.TestUtil.runBeaver;
import static org.extendj.neobeaver.TestUtil.runJFlex;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class TestParsing {
  private static final Path TMP_PATH = Paths.get("tmp");
  private final String name;
  private final Path testRoot;
  private static final Set<String> neoOptions = new HashSet<>(Arrays.asList("--beaver", "--unreachable-error"));

  public TestParsing(String name) {
    this.name = name;
    testRoot = Paths.get("testfiles", name);
  }

  @Parameterized.Parameters(name = "{0}")
  public static Iterable<Object[]> getTests() {
    ArrayList<Object[]> tests = new ArrayList<>();
    File testRoot = new File("testfiles");
    File[] files = testRoot.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.isDirectory()) {
          tests.add(new Object[] { file.getName() });
        }
      }
    }
    return tests;
  }

  // TODO: redirect beaver output to file.
  @Test public void testBeaver() throws Exception {
    Path testDir = TMP_PATH.resolve(name + "-beaver");
    if (testDir.toFile().isDirectory()) {
      // Clean old test files.
      deleteDirectory(testDir);
    }
    Path parserSpec = findParser();
    Properties testProperties = readProperties(parserSpec);
    Path scannerSpec = findScanner();
    Path packageDir = testDir.resolve("org/extendj/parsertest");
    packageDir.toFile().mkdirs();
    runBeaver(testDir, testDir.toString(), parserSpec.toString());
    File parserFile = packageDir.resolve("Parser.java").toFile();
    if (parserFile.isFile()) {
      if (testProperties.getProperty("beaver.result", "").equals("ERROR")) {
        fail("Beaver generated a parser successfully when expected to fail.");
      }
    } else {
      if (!testProperties.getProperty("beaver.result", "").equals("ERROR")) {
        fail("Beaver failed to generate a parser when expected to succeed.");
      }
      return;
    }
    runJFlex(packageDir.toString(), scannerSpec.toString());
    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    int compileResult = compiler.run(null, System.out, System.err,
        "-classpath",
        "libs/beaver-rt-0.9.11.jar",
        "-d",
        testDir.toAbsolutePath().toString(),
        parserFile.getAbsolutePath(),
        packageDir.resolve("Scanner.java").toAbsolutePath().toString());
    if (compileResult == 0) {
      if (testProperties.getProperty("beaver.result", "").equals("COMPILE_ERROR")) {
        fail("Generated parser passed compilation when expected to fail.");
      }
    } else {
      if (testProperties.getProperty("beaver.result", "").equals("COMPILE_ERROR")) {
        return;
      } else {
        fail("Generated parser failed to compile when expected to pass compilation.");
      }
    }
    URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
        testDir.toUri().toURL(),
        Paths.get("libs/beaver-rt-0.9.11.jar").toUri().toURL()
    });
    Class<?> parser = Class.forName("org.extendj.parsertest.Parser", true, classLoader);
    Class<?> beaverScanner = Class.forName("beaver.Scanner", true, classLoader);
    Class<?> scanner = Class.forName("org.extendj.parsertest.Scanner", true, classLoader);
    Constructor<?> scannerCons = scanner.getConstructor(Reader.class);
    Method parseMethod = parser.getMethod("parse", beaverScanner);
    checkOutput(testDir, parser, scannerCons, parseMethod);
  }

  private Properties readProperties(Path path) throws IOException {
    Properties properties = new Properties();
    try (java.util.Scanner scanner = new java.util.Scanner(new FileInputStream(path.toFile()))) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        if (line.startsWith("//")) {
          int index = line.indexOf('.');
          if (index == 2 || index == 3) {
            // This line contains a test property.
            String property = line.substring(index + 1).trim();
            if (!property.isEmpty()) {
              properties.load(new StringReader(property));
            }
          }
        }
      }
    }
    return properties;
  }

  @Test public void testNeo() throws Exception {
    Path testDir = TMP_PATH.resolve(name + "-neo");
    if (testDir.toFile().isDirectory()) {
      // Clean old test files.
      deleteDirectory(testDir);
    }
    Path parserSpec = findParser();
    Properties testProperties = readProperties(parserSpec);
    Path scannerSpec = findScanner();
    Files.createDirectories(testDir);
    // Redirect Standard out to file.
    boolean errored;
    try {
      // Redirect output streams.
      PrintStream standardOut = System.out;
      PrintStream standardErr = System.err;
      try (PrintStream out = new PrintStream(testDir.resolve("cc.out").toFile());
          PrintStream err = new PrintStream(testDir.resolve("cc.err").toFile())) {
        System.setOut(out);
        System.setErr(err);
        errored = NeoBeaver.run(Collections.singleton(parserSpec.toString()), testDir, neoOptions);
        if (errored && !testProperties.getProperty("neo.result", "").equals("ERROR")) {
          fail("NeoBeaver failed when expected to succeed.");
          return;
        }
      } finally {
        System.setOut(standardOut);
        System.setErr(standardErr);
      }
    } catch (Error | IOException e) {
      if (!testProperties.getProperty("neo.result", "").equals("ERROR")) {
        fail("NeoBeaver failed when expected to succeed. Error: " + e.getMessage());
      }
      return;
    }
    Path packageDir = testDir.resolve("org/extendj/parsertest");
    File parserFile = packageDir.resolve("Parser.java").toFile();
    if (parserFile.isFile()) {
      if (testProperties.getProperty("neo.result", "").equals("ERROR")) {
        fail("NeoBeaver generated a parser successfully when expected to fail.");
      }
    } else {
      if (!testProperties.getProperty("neo.result", "").equals("ERROR")) {
        fail("NeoBeaver failed to generate a parser when expected to succeed.");
      }
      return;
    }
    runJFlex(packageDir.toString(), scannerSpec.toString());
    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    int compileResult = compiler.run(null, System.out, System.err,
        "-classpath",
        "libs/beaver-rt-0.9.11.jar",
        "-d",
        testDir.toAbsolutePath().toString(),
        parserFile.getAbsolutePath(),
        packageDir.resolve("Scanner.java").toAbsolutePath().toString());
    if (compileResult == 0) {
      if (testProperties.getProperty("neo.result", "").equals("COMPILE_ERROR")) {
        fail("Generated parser passed compilation when expected to fail.");
      }
    } else {
      if (testProperties.getProperty("neo.result", "").equals("COMPILE_ERROR")) {
        return;
      } else {
        fail("Generated parser failed to compile when expected to pass compilation.");
      }
    }
    URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
        testDir.toUri().toURL(),
        Paths.get("libs/beaver-rt-0.9.11.jar").toUri().toURL()
    });
    Class<?> parser = Class.forName("org.extendj.parsertest.Parser", true, classLoader);
    Class<?> beaverScanner = Class.forName("beaver.Scanner", true, classLoader);
    Class<?> scanner = Class.forName("org.extendj.parsertest.Scanner", true, classLoader);
    Constructor<?> scannerCons = scanner.getConstructor(Reader.class);
    Method parseMethod = parser.getMethod("parse", beaverScanner);
    checkOutput(testDir, parser, scannerCons, parseMethod);
  }

  private void checkOutput(Path testDir, Class<?> parser, Constructor<?> scannerConstructor, Method parseMethod)
      throws IOException {
    boolean found = false;
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(testRoot)) {
      for (Path input : stream) {
        if (input.toString().endsWith(".in")) {
          found = true;
          String inputName = input.getFileName().toString();
          String expectedName = inputName.substring(0, inputName.length() - 3) + ".expected";
          String outputName = inputName.substring(0, inputName.length() - 3) + ".out";
          Path expectedPath = input.resolveSibling(expectedName);
          String actual = "", expected = "?";
          try {
            Object res = parseMethod.invoke(parser.newInstance(),
                scannerConstructor.newInstance(new FileReader(input.toFile())));
            actual = res.toString();
            Files.write(testDir.resolve(outputName), actual.getBytes(),
                StandardOpenOption.CREATE_NEW);
            expected = new String(Files.readAllBytes(expectedPath));
          } catch (InvocationTargetException e) {
            Throwable error = e.getTargetException();
            fail(String.format("Failed to parse input file %s: (%s) %s", inputName,
                error.getClass().getName(), error.getMessage()));
          } catch (Exception e) {
            fail(String.format("Failed to parse input file %s: (%s) %s", inputName,
                e.getClass().getName(), e.getMessage()));
          }
          assertEquals("[" + inputName + "]", normalize(expected), normalize(actual));
        }
      }
    }
    if (!found) {
      fail("Found no input files for test " + testRoot);
    }
  }

  private Path findParser() throws IOException {
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(testRoot)) {
      for (Path path : stream) {
        if (path.toString().endsWith(".lr")) {
          return path;
        }
      }
    }
    throw new Error("Could not find parser specification for test " + testRoot);
  }

  private Path findScanner() throws IOException {
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(testRoot)) {
      for (Path path : stream) {
        if (path.toString().endsWith(".jflex")) {
          return path;
        }
      }
    }
    throw new Error("Could not find scanner specification for test " + testRoot);
  }
}
