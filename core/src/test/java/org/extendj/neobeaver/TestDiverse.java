/*Copyright (c) 2019, Filip Johansson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/


package org.extendj.neobeaver;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import static org.extendj.neobeaver.Util.parse;
import org.extendj.neobeaver.ast.Terminals;
import org.extendj.neobeaver.ast.List;
import org.extendj.neobeaver.ast.GSym;
import org.extendj.neobeaver.ast.GDecl;
import org.extendj.neobeaver.ast.GGrammar;
import org.extendj.neobeaver.ast.GProduction;
import org.extendj.neobeaver.ast.Opt;
import org.extendj.neobeaver.ast.GRule;
import org.extendj.neobeaver.ast.GComponent;


import java.io.File;
/**
 * This file is used to test simple things
 */
public class TestDiverse {
	private static final File TEST_DIRECTORY = new File("testfiles/gen_rule_tests");

    // testing if the parser can parser the grammar
    @Test public void test_parseGrammar() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());
    }

	// testing terminals attribute for GGrammar
	@Test public void test_terminalsAttribute() throws Exception {
		GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

		assertEquals(grammar.terminals().contains("factor"), false);
        assertEquals(grammar.terminals().contains("parameter"), false);
		assertEquals(grammar.terminals().contains("EQUAL"), true);
        assertEquals(grammar.terminals().contains("NUMERAL"), true);
        assertEquals(grammar.terminals().contains("ASSIGN"), true);
  	}

    // testing nonTerminals attribute for GGrammar
    @Test public void test_nonTerminalsAttribute() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

        assertEquals(grammar.nonTerminals().contains("comparison"), true);
        assertEquals(grammar.nonTerminals().contains("argumet_list"), true);
        assertEquals(grammar.nonTerminals().contains("LEFTPAR"), false);
        assertEquals(grammar.nonTerminals().contains("SEMICOL"), false);
        assertEquals(grammar.nonTerminals().contains("COMMA"), false);
    }

    // testing symbols attribute for GGrammar
    @Test public void test_symbols() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

        assertEquals(grammar.symbols().contains("comparison"), true);
        assertEquals(grammar.symbols().contains("argumet_list"), true);
        assertEquals(grammar.symbols().contains("LEFTPAR"), true);
        assertEquals(grammar.symbols().contains("SEMICOL"), true);
        assertEquals(grammar.symbols().contains("COMMA"), true);

        assertEquals(grammar.symbols().contains("factor"), true);
        assertEquals(grammar.symbols().contains("parameter"), true);
        assertEquals(grammar.symbols().contains("EQUAL"), true);
        assertEquals(grammar.symbols().contains("NUMERAL"), true);
        assertEquals(grammar.symbols().contains("ASSIGN"), true);
    }

    // testing isStartSymbol attribute for GGrammar
    @Test public void test_isStartSymbol() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

        assertEquals(grammar.isStartSymbol("program"), true);
        assertEquals(grammar.isStartSymbol("factor"), false);
    }

    // testing isRule attribute for GRule
    @Test public void test_isRule() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

        for (GRule rule : grammar.rules()) {
            assertEquals(rule.isRule(), true);
        }

        GRule r = grammar.ready();
        GRule u = grammar.unsure();
        GRule f = grammar.finished();

        assertEquals(r.isRule(), false);
        assertEquals(u.isRule(), false);
        assertEquals(f.isRule(), false);
    }

    // testing nullable() attribute for GGrammar
    @Test public void test_nullable() throws Exception {
        GGrammar grammar = (GGrammar) parse((new File(TEST_DIRECTORY, "parser.beaver")).toPath());

        assertEquals(grammar.nullable("program"), true);
        assertEquals(grammar.nullable("function_list"), true);
        assertEquals(grammar.nullable("statement_list"), true);
        assertEquals(grammar.nullable("INT"), false);
        assertEquals(grammar.nullable("block"), false);
        assertEquals(grammar.nullable("assignment"), false);
    }

    // testing first() attribute for GGrammar
    @Test public void test_first() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "testFirst.beaver").toPath());

        assertEquals(grammar.nullable("X"), true);
        assertEquals(grammar.nullable("Y"), true);
        assertEquals(grammar.nullable("Z"), false);

        assertEquals(grammar.first("X").contains("a"), true);
        assertEquals(grammar.first("X").contains("c"), true);
        assertEquals(grammar.first("X").contains("d"), false);
        assertEquals(grammar.first("Y").contains("a"), false);
        assertEquals(grammar.first("Y").contains("c"), true);
        assertEquals(grammar.first("Y").contains("d"), false);
        assertEquals(grammar.first("Z").contains("a"), true);
        assertEquals(grammar.first("Z").contains("c"), true);
        assertEquals(grammar.first("Z").contains("d"), true);

    }

    // testing follow() attribute for GGrammar
    @Test public void test_follow() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "testFollow.beaver").toPath());

        assertEquals(grammar.follow("S").contains("a"), false);
        assertEquals(grammar.follow("S").contains("c"), false);
        assertEquals(grammar.follow("S").contains("d"), false);
        assertEquals(grammar.follow("S").contains("END"), false);

        assertEquals(grammar.follow("X").contains("a"), true);
        assertEquals(grammar.follow("X").contains("c"), true);
        assertEquals(grammar.follow("X").contains("d"), true);
        assertEquals(grammar.follow("X").contains("END"), false);

        assertEquals(grammar.follow("Y").contains("a"), true);
        assertEquals(grammar.follow("Y").contains("c"), true);
        assertEquals(grammar.follow("Y").contains("d"), true);
        assertEquals(grammar.follow("Y").contains("END"), false);

        assertEquals(grammar.follow("Z").contains("a"), false);
        assertEquals(grammar.follow("Z").contains("c"), false);
        assertEquals(grammar.follow("Z").contains("d"), false);
        assertEquals(grammar.follow("Z").contains("END"), true);
    }

    // testing last() attribute for GGrammar
    @Test public void test_last() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "parser.beaver").toPath());

        assertEquals(grammar.last("ID").contains("ID"), true);
        assertEquals(grammar.last("ID").contains("ADD"), false);
        assertEquals(grammar.last("MUL").contains("MUL"), true);
        assertEquals(grammar.last("MUL").contains("statement"), false);

        assertEquals(grammar.last("statement").contains("SEMICOL"), true);
        assertEquals(grammar.last("statement").contains("RIGHTBRACE"), true);
        assertEquals(grammar.last("statement").contains("LEFTBRACE"), false);
        assertEquals(grammar.last("statement").contains("ASSIGN"), false);

        assertEquals(grammar.last("program").contains("SEMICOL"), false);
        assertEquals(grammar.last("statement").contains("RIGHTBRACE"), true);

        assertEquals(grammar.last("expression").contains("RIGHTBRACE"), false);
        assertEquals(grammar.last("expression").contains("ID"), true);
        assertEquals(grammar.last("expression").contains("ASSIGN"), false);
        assertEquals(grammar.last("expression").contains("LEFTBRACE"), false);
        assertEquals(grammar.last("expression").contains("NUMERAL"), true);

    }

    // testing precede() attribute for GGrammar
    @Test public void test_precede() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "parser.beaver").toPath());

        assertEquals(grammar.precede("statement").contains("LEFTBRACE"), true);
        assertEquals(grammar.precede("statement").contains("RIGHTBRACE"), true);
        assertEquals(grammar.precede("statement").contains("SEMICOL"), true);
        assertEquals(grammar.precede("statement").contains("LEFTPAR"), false);
        assertEquals(grammar.precede("statement").contains("RIGHTPAR"), false);

        assertEquals(grammar.precede("expression").contains("RIGHTPAR"), false);
        assertEquals(grammar.precede("expression").contains("LEFTPAR"), true);

        assertEquals(grammar.precede("parameter").contains("COMMA"), true);
        assertEquals(grammar.precede("parameter").contains("LEFTPAR"), true);
        assertEquals(grammar.precede("parameter").contains("ADD"), false);

    }

    // testing left() attribute for GRule
    @Test public void test_left() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "testFollow.beaver").toPath());

        for (GRule rule : grammar.rules()) {
            if (rule.leftSide().equals("S")) {
                assertEquals(rule.left(0).contains("a"), false);
                assertEquals(rule.left(0).contains("c"), false);
                assertEquals(rule.left(0).contains("d"), false);
                assertEquals(rule.left(0).contains("END"), false);

                assertEquals(rule.left(1).contains("a"), false);
                assertEquals(rule.left(1).contains("c"), false);
                assertEquals(rule.left(1).contains("d"), true);
                assertEquals(rule.left(1).contains("END"), false);

                assertEquals(rule.left(2).contains("a"), false);
                assertEquals(rule.left(2).contains("c"), false);
                assertEquals(rule.left(2).contains("d"), false);
                assertEquals(rule.left(2).contains("END"), true);
            }
        }
    }

    // testing right() attribute for GRule
    @Test public void test_right() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "testFollow.beaver").toPath());

        for (GRule rule : grammar.rules()) {
            if (rule.leftSide().equals("S")) {
                assertEquals(rule.right(0).contains("a"), true);
                assertEquals(rule.right(0).contains("c"), true);
                assertEquals(rule.right(0).contains("d"), true);
                assertEquals(rule.right(0).contains("END"), false);

                assertEquals(rule.right(1).contains("a"), false);
                assertEquals(rule.right(1).contains("c"), false);
                assertEquals(rule.right(1).contains("d"), false);
                assertEquals(rule.right(1).contains("END"), true);

                assertEquals(rule.right(2).contains("a"), false);
                assertEquals(rule.right(2).contains("c"), false);
                assertEquals(rule.right(2).contains("d"), false);
                assertEquals(rule.right(2).contains("END"), false);
            }
        }
    }

    // testing isPP() attribute for GGrammar
    @Test public void test_isPP() throws Exception {
        GGrammar grammar = (GGrammar) parse(new File(TEST_DIRECTORY, "parser.beaver").toPath());

        assertEquals(grammar.isPP("COMMA", "RIGHTPAR"), true);
        assertEquals(grammar.isPP("RIGHTPAR", "COMMA"), false);

        assertEquals(grammar.isPP("ELSE", "RETURN"), true);
        assertEquals(grammar.isPP("SEMICOL", "ID"), false);
    }

    // tests isInf for InfInt
    @Test public void test_isInf() throws Exception {
        InfInt uut = new InfInt(true);

        assertEquals(uut.isInf(), true);
    }

    // testing add for InfInt
    @Test public void test_add() throws Exception {
        InfInt uut1 = new InfInt(true);
        InfInt uut2 = new InfInt(5);
        InfInt uut3 = new InfInt(17);

        uut1.add(uut2);
        assertEquals(uut1.isInf(), true);

        uut2.add(uut3);
        assertEquals(uut2.getValue(), 22);
        assertEquals(uut2.isInf(), false);

        uut3.add(uut1);
        assertEquals(uut3.isInf(), true);
    }

    // testing sub for InfInt
    @Test public void test_sub() throws Exception {
        InfInt uut1 = new InfInt(true);
        InfInt uut2 = new InfInt(5);
        InfInt uut3 = new InfInt(17);

        uut1.sub(uut2);
        assertEquals(uut1.isInf(), true);

        uut2.sub(uut3);
        assertEquals(uut2.getValue(), -12);
        assertEquals(uut2.isInf(), false);

        uut3.sub(uut1);
        assertEquals(uut3.isInf(), true);
    }

    // testing lessThan for InfInt
    @Test public void test_lessThan() throws Exception {
        InfInt uut1 = new InfInt(true);
        InfInt uut2 = new InfInt(5);
        InfInt uut3 = new InfInt(17);

        InfInt uut4 = new InfInt(5);
        InfInt uut5 = new InfInt(true);

        assertEquals(uut1.lessThan(uut2), false);
        assertEquals(uut1.lessThan(uut1), false);

        assertEquals(uut2.lessThan(uut1), true);
        assertEquals(uut2.lessThan(uut3), true);

        assertEquals(uut3.lessThan(uut2), false);
        assertEquals(uut3.lessThan(uut1), true);

        assertEquals(uut4.lessThan(uut2), false);
        assertEquals(uut5.lessThan(uut1), false);

    }






	
}
