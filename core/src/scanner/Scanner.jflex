package org.extendj.neobeaver;

import static org.extendj.neobeaver.Parser.Tokens.*;

import org.extendj.neobeaver.Parser.Token;
import org.extendj.neobeaver.Parser.Tokens;
import org.extendj.neobeaver.Parser.TokenScanner;
import org.extendj.neobeaver.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%implements TokenScanner
%implements AutoCloseable

%type Token
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  StringBuilder stringbuf = new StringBuilder(128);

  private Token sym(int id) {
    return new Token(id, yytext(), pos());
  }

  private Token sym(int id, String text) {
    return new Token(id, text, pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }

  @Override public void close() throws java.io.IOException {
  	yyclose();
  }
%}

/* Line Terminators. */
LineTerminator = \n|\r|\r\n
InputCharacter = [^\r\n]
StringCharacter = [^\r\n\"\\]

/* White Space */
WhiteSpace = [ ] | \t | \f | {LineTerminator}

/* Comments. */
Comment = {TraditionalComment} | {EndOfLineComment}

TraditionalComment = "/*" ~"*/"
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?

%state CODE
%state STRING

%%

/* Comments & whitespace. */
<YYINITIAL> {
  {WhiteSpace} { }
  {Comment} { }
}

<YYINITIAL> {
  "%header" { return sym(HEADER); }
  "%embed" { return sym(EMBED); }
  "%goal" { return sym(GOAL); }
  "%class" { return sym(CLASS); }
  "%package" { return sym(PACKAGE); }
  "%left" { return sym(LEFT); }
  "%right" { return sym(RIGHT); }
  "%nonassoc" { return sym(NONASSOC); }
  "%terminals" { return sym(TERMINALS); }
  "%typeof" { return sym(TYPEOF); }
  ","   { return sym(COMMA); }
  "="   { return sym(EQ); }
  ":="  { return sym(COLONEQ); }
  "*"   { return sym(ASTERISK); }
  "+"   { return sym(PLUS); }
  "?"   { return sym(QUESTION); }
  "|"   { return sym(OR); }
  "{:"  { yybegin(CODE); return sym(CODE_START); }
  ";"   { return sym(SEMICOLON); }
  "."   { return sym(DOT); }
  "@"   { return sym(AT); }
  \"    { stringbuf.setLength(0); yybegin(STRING); }
}

<CODE> {
  ([^:]+ | ":" [^}])+ { return sym(JAVA_CODE); }
  ":}"  { yybegin(YYINITIAL); return sym(CODE_END); }
}

<STRING> {
  \"  { yybegin(YYINITIAL); return sym(Tokens.STRING, stringbuf.toString()); }
  {StringCharacter}+ { stringbuf.append(yytext()); }
  "\\b"              { stringbuf.append('\b'); }
  "\\t"              { stringbuf.append('\t'); }
  "\\n"              { stringbuf.append('\n'); }
  "\\f"              { stringbuf.append('\f'); }
  "\\r"              { stringbuf.append('\r'); }
  "\\\""             { stringbuf.append('\"'); }
  "\\'"              { stringbuf.append('\''); }
  "\\\\"             { stringbuf.append('\\'); }

  \\.                            { error("illegal escape sequence \"" + yytext() + "\""); }
  {LineTerminator}               { error("unterminated string at end of line"); }
}

/* Identifiers. */
<YYINITIAL> {
  ([:jletter:]|[\ud800-\udfff])([:jletterdigit:]|[\ud800-\udfff])* {
    return sym(ID);
  }
}

/* Fall through errors. */
[^] {
  error("illegal character \"" + yytext() +  "\"");
}

<<EOF>> {
  return new Token(EOF, "EOF", pos());
}
