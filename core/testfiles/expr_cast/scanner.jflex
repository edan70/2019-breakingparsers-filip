package org.extendj.parsertest;

import static org.extendj.parsertest.Parser.Terminals.*;

import org.extendj.parsertest.Parser.Token;
import org.extendj.parsertest.Parser.Terminals;
import org.extendj.parsertest.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  private Token sym(int id) {
    return new Token(id, yytext(), pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r
ID = [a-zA-Z]+
INT = [0-9]+

%%

<YYINITIAL> {
  {WhiteSpace} { }
  {ID}  { return sym(ID); }
  {INT} { return sym(INT); }
  "("   { return sym(LPAREN); }
  ")"   { return sym(RPAREN); }
  "*"   { return sym(ASTERISK); }
}

/* Fall through errors. */
[^] {
  error("illegal character \"" + yytext() +  "\"");
}

<<EOF>> {
  return new Token(EOF, "EOF", pos());
}
