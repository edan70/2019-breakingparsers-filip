// .beaver.result = ERROR
// .neo.result = ERROR
%package "org.extendj.parsertest";

%class "Parser";

%embed {:
  static public class SyntaxError extends RuntimeException {
    public SyntaxError(String msg) {
      super(msg);
    }
  }

  public static class SourcePosition {
    int line, column;

    public SourcePosition(int line, int column) {
      this.line = line;
      this.column = column;
    }

    @Override
    public String toString() {
      return "" + line + ":" + column;
    }
  }

  public static class Token extends Symbol {
    public final String literal;
    public final int id;
    public final SourcePosition position;

    public Token(int id, String literal, SourcePosition pos) {
      super((short) id, literal);
      this.literal = literal;
      this.id = id;
      this.position = pos;
    }

    @Override public String toString() {
      return literal;
    }

    public SourcePosition getPosition() {
      return position;
    }
  }

  public static class Nonterminal extends Symbol {
    public final String name;
    public final Symbol[] children;

    public Nonterminal(String name, Symbol... children) {
      this.name = name;
      this.children = children;
    }

    void dump(StringBuilder result, String prefix) {
      result.append(name).append(":");
      if (children.length > 1) {
        prefix += "  ";
      }
      for (Symbol child : children) {
        if (children.length > 1) {
          result.append("\n").append(prefix);
        } else {
          result.append(" ");
        }
        if (child instanceof Nonterminal) {
          ((Nonterminal) child).dump(result, prefix);
        } else if (child instanceof Token) { // Avoid empty optional.
          result.append(child.toString());
        }
      }
    }


    @Override public String toString() {
      StringBuilder result = new StringBuilder();
      dump(result, "");
      return result.toString();
    }
  }
:};

// Test reduce-reduce conflict.

%terminals ADD;
%terminals ID;

%goal expr;

expr =
  | add1.add
  | add2.sub
  | ID.ID {: return new Nonterminal("expr", ID); :}
  ;

add1 =
    expr.a ADD.op expr.b {: return new Nonterminal("expr", a, op, b); :}
  ;

add2 =
    expr.a ADD.op expr.b {: return new Nonterminal("expr", a, op, b); :}
  ;
