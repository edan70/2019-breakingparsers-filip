package org.extendj.parsertest;

import static org.extendj.parsertest.Parser.Terminals.*;

import org.extendj.parsertest.Parser.Token;
import org.extendj.parsertest.Parser.Terminals;
import org.extendj.parsertest.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  private Token sym(short id) {
    return new Token(id, yytext(), pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }

  int commentDepth = 0;

  private void enterComment() {
    if (commentDepth == 0) {
      yybegin(COMMENT);
    }
    commentDepth += 1;
  }

  private void leaveComment() {
    commentDepth -= 1;
    if (commentDepth == 0) {
      yybegin(YYINITIAL);
    }
  }
%}

eol = \n|\r
input_char = [^\r\n]

whitespace = [ ] | \t | \f | {eol}

eol_comment = "//" {input_char}* {eol}?

digit = [0-9]
num = {digit}+
id = [_a-zA-Z][_a-zA-Z0-9]*

line_terminator = \n|\r|\r\n

%state COMMENT

%%

<YYINITIAL> {

  {whitespace} { }
  {eol_comment} { }

  "+"          { return sym(Terminals.ADD); }
  "-"          { return sym(Terminals.SUB); }

  "/*"         { enterComment(); }

  {id}         { return sym(Terminals.ID); }
}

<COMMENT> {
  "/*"         { enterComment(); }
  "*/"         { leaveComment(); }
  [^]          { }
}

[^]            { error("Illegal character <" + yytext() + ">"); }
<<EOF>>        { return sym(Terminals.EOF); }
