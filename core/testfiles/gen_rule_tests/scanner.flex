package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace = [ ] | \t | \f | \n | \r
Comment = "//"[^\n]*\n
Multiline_Comment = \/\* [^]*? \*\/
ID = [a-zA-Z][a-zA-Z0-9]*
NUMERAL = [0-9]+

%%

// discard whitespace information
{WhiteSpace}        { }
{Comment}           { }
{Multiline_Comment} { }

// token definitions
"int"         { return sym(Terminals.INT); }
"if"          { return sym(Terminals.IF); }
"while"       { return sym(Terminals.WHILE); }
"else"        { return sym(Terminals.ELSE); }
"("           { return sym(Terminals.LEFTPAR); }
")"           { return sym(Terminals.RIGHTPAR); }
"{"           { return sym(Terminals.LEFTBRACE); }
"}"           { return sym(Terminals.RIGHTBRACE); } 
"="           { return sym(Terminals.ASSIGN); }    
";"           { return sym(Terminals.SEMICOL); }
","           { return sym(Terminals.COMMA); }
"+"           { return sym(Terminals.ADD); }
"-"           { return sym(Terminals.SUB); }
"*"           { return sym(Terminals.MUL); }     
"/"           { return sym(Terminals.DIV); }     
"%"           { return sym(Terminals.REST); }     
"=="          { return sym(Terminals.EQUAL); }
"!="          { return sym(Terminals.NOTEQUAL); }
"<="          { return sym(Terminals.LESSEQUAL); }  
"<"           { return sym(Terminals.LESS); }  
">="          { return sym(Terminals.GREATEREQUAL); }  
">"           { return sym(Terminals.GREATER); }
"return"      { return sym(Terminals.RETURN); }
{ID}          { return sym(Terminals.ID); }
{NUMERAL}     { return sym(Terminals.NUMERAL); }
<<EOF>>       { return sym(Terminals.EOF); }

/* error fallback */
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
