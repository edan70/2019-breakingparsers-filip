package org.extendj.parsertest;

import static org.extendj.parsertest.Parser.Terminals.*;

import org.extendj.parsertest.Parser.Token;
import org.extendj.parsertest.Parser.Terminals;
import org.extendj.parsertest.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%extends beaver.Scanner

%type beaver.Symbol 
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  private Token sym(int id) {
    return new Token(id, yytext(), pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }
%}

WhiteSpace = ([ ] | \t | \f | \n | \r)+
Identifier = [a-zA-Z]+
Numeral = [0-9]+

%%

<YYINITIAL> {
  {WhiteSpace}    { }
  "="             { return sym(ASSIGN); }
  "*"             { return sym(MUL); }
  "/"             { return sym(DIV); }
  "let"           { return sym(LET); }
  "in"            { return sym(IN); }
  "end"           { return sym(END); }
  "ask"           { return sym(ASK); }
  "user"          { return sym(USER); }
  { Identifier }  { return sym(ID); }
  { Numeral }     { return sym(NUMERAL); }
}

/* Fall through errors. */
[^] {
  error("illegal character \"" + yytext() +  "\"");
}

<<EOF>> {
  return new Token(EOF, "EOF", pos());
}
