package beaver.comp.run;

import org.extendj.neobeaver.NeoBeaver;
import org.extendj.neobeaver.ProblemLogger;
import org.extendj.neobeaver.TraceHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;

public class Make {
  public static void main(String[] args) throws IOException {
    System.out.println("Welcome to NeoBeaver classic frontend!");
    ProblemLogger problems = new ProblemLogger();
    try {
      String destDirName = "";
      String filename = "";

      for (int i = 0; i < args.length && !problems.errored(); ++i) {
        String arg = args[i];
        switch (arg) {
          case "-h":
            System.out.println("Usage: nbfront [OPTIONS] <PARSER SPEC FILE>");
            System.out.println("Options:");
            System.out.println("  -d <DIR>    Generate parser in the named directory, DIR.");
            System.out.println("  -h          Print help and exit.");
            return;
          case "-d":
            if (args.length > i + 1) {
              destDirName = args[i + 1];
              i += 1;
            } else {
              problems.error("missing path argument to -d option.");
              return;
            }
            break;
          case "-t":
            // Generate terminal names.
            // TODO: implement (always on right now).
            break;
          case "-c":
            // Suppress parsing table compression.
            // TODO: implement.
            break;
          case "-w":
            // Use switch to invoke reduce actions.
            // TODO: implement.
            break;
          default:
            if (arg.startsWith("-")) {
              problems.warnf("ignoring option '%s'. It is probably an unsupported Beaver flag.", arg);
            } else {
              if (filename.isEmpty()) {
                filename = arg;
              } else {
                problems.warnf("multiple filenames specified. Ignoring file: %s", arg);
              }
            }
        }
      }

      if (filename.isEmpty()) {
        problems.error("no parser specification filename specified!");
        return;
      }

      TraceHandler trace = NeoBeaver.nullTrace();
      Set<String> options = Collections.singleton("--beaver");
      NeoBeaver.process(trace, problems, Paths.get(filename), Paths.get(destDirName), options);

      problems.report();
    } finally {
      problems.report();
      if (problems.errored()) {
        System.exit(1);
      }
    }
  }
}
